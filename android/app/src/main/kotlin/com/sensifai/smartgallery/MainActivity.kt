package com.sensifai.smartgallery

import android.content.Context
import android.content.res.AssetManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import com.sensifai.smartgallery.ml.Model
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import org.tensorflow.lite.DataType
import org.tensorflow.lite.support.image.TensorImage
import org.tensorflow.lite.support.tensorbuffer.TensorBuffer
import java.io.IOException
import java.io.InputStream


class MainActivity: FlutterActivity() {

    var channel = "com.sensifai.smartgallery/ai"

    override fun configureFlutterEngine(flutterEngine: FlutterEngine) {
        super.configureFlutterEngine(flutterEngine)
        val model = Model.newInstance(this@MainActivity)
        MethodChannel(flutterEngine.dartExecutor.binaryMessenger, channel).setMethodCallHandler { call, result ->
            when(call.method){
                "run" -> runModel(call, result, model)
                else -> throw NotImplementedError("method not found")
            }
        }
    }


    private fun runModel(call: MethodCall, result: MethodChannel.Result, model: Model) {

        val byteArrayImage: ByteArray = call.argument<ByteArray>("byteImage") ?: return

//
        val inputFeature0 = TensorBuffer.createFixedSize(intArrayOf(512, 512, 3), DataType.UINT8)
//
//        // START: Create Bitmap
//
        val bitmap = BitmapFactory.decodeByteArray(byteArrayImage, 0, byteArrayImage.size)
//
//        // END: Creating Bitmap
//
        val image = TensorImage.fromBitmap(Bitmap.createScaledBitmap(bitmap, 512, 512, true))
        inputFeature0.loadBuffer(image.buffer)
//
//
        val outputs = model.process(inputFeature0)
        val outputFeature0 = outputs.outputFeature0AsTensorBuffer
//
//        val file_name = "labels.txt"
        val responses = outputFeature0.floatArray
        val x = ArrayList<Confidence>()
        val labels = Labels(this@MainActivity)
        for (i in 0 until responses.size){
            if (responses[i] > 0.3f){
                val label = labels.getLabelByIndex(i)
                val description = labels.getDescriptionByLabel(label)
                x.add(Confidence(i, responses[i], label, description))
            }
        }

        x.sortWith { o1, o2 ->
            o2.rate.compareTo(o1.rate)
        }

        var response = "[\n"
        for (i in 0 until x.size){
            response += "   {\n"
            response += "       \"index\": " + x[i].index + ",\n"
            response += "       \"confidence\": " + x[i].rate.toDouble() + ",\n"
            response += "       \"label\": \"" + x[i].label + "\",\n"
            response += "       \"description\": \"" + x[i].description + "\"\n"
            response += "   }"
            response += if ((x.size - 1) != i){
                ",\n"
            } else {
                "\n"
            }
        }

        response += "]"

        if (x.isNotEmpty()){
            result.success(response)
        } else {
            result.success("[]")
        }
    }

    private fun getBitmapFromAsset(context: Context, filePath: String): Bitmap? {
        val assetManager: AssetManager = context.assets
        val istr: InputStream
        var bitmap: Bitmap? = null
        try {
            istr = assetManager.open(filePath)
            bitmap = BitmapFactory.decodeStream(istr)
        } catch (e: IOException) {
            // handle exception
        }
        return bitmap
    }

}
