### How to Install and use the Smart Gallery App?

#### 1. Click on the package installer to download the .apk file

<img src="assets/images/guide/m.jpg" width="320">
<img src="assets/images/guide/2.jpg" width="320">

#### 2. Click on the Install button 

<img src="assets/images/guide/3.jpg" width="320">
<img src="assets/images/guide/4.jpg" width="320">

#### 3.  Grant permission to access your gallery 

<img src="assets/images/guide/5.jpg" width="220">

#### 4. Grant permission to access your internal storage.

<img src="assets/images/guide/6.jpg" width="220">
<img src="assets/images/guide/7.jpg" width="225">

#### 5. Move the scroll bar to grant permission to Draw Over Other Apps.

<img src="assets/images/guide/8.jpg" width="220">

#### 6. Well done! The installation has finished, and tagging process has started. As tagging process starts, you can see the notification on your phone for the tagging process of the album. 
Please Note: Be patient process may take several minutes depending on the album size. You cannot see the label for the images until the end of the process.

<img src="assets/images/guide/9.jpg" width="420">

#### 7. Open a photo and see the labels at the bottom of the page. You can add or delete the tag as you deem appropriate.

<img src="assets/images/guide/10.jpg" width="220">

#### 8. To back up the photos on Dropbox, enter the setting and click Dropbox Account and then Add Account. Sign in and link the Smart Gallery App to your Dropbox account.

<img src="assets/images/guide/11.jpg" width="230">
<img src="assets/images/guide/12.jpg" width="220">
<img src="assets/images/guide/13.jpg" width="220">
<img src="assets/images/guide/14.jpg" width="220">

#### 9. To upload photos, open your desired album, select the image and click on the upload icon on the top right side of the page.

<img src="assets/images/guide/15.jpg" width="220">
<img src="assets/images/guide/16.jpg" width="220">

#### 10. To download photos, click the download icon on the top right side of the page, follow the path where the image is uploaded, and click download.

<img src="assets/images/guide/17.jpg" width="220">
<img src="assets/images/guide/18.jpg" width="220">
<img src="assets/images/guide/19.jpg" width="220">
<img src="assets/images/guide/20.jpg" width="220">

#### 11. To see the downloaded photos click the icon on the top right side of the page.

<img src="assets/images/guide/21.jpg" width="220">
<img src="assets/images/guide/22.jpg" width="220">
