// import 'package:awesome_select/awesome_select.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:group_radio_button/group_radio_button.dart';
import 'package:provider/provider.dart';
import 'package:settings_ui/settings_ui.dart';
import 'package:sizer/sizer.dart';
import 'package:smartgallery/theme/widgets/tm_text.dart';

import '../../provider/notifiers/settings/settings_viewmodel.dart';


class SettingsScreen extends StatefulWidget {
  const SettingsScreen({Key? key}) : super(key: key);

  @override
  State<SettingsScreen> createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  
  
  bool development = false ;

  @override
  void initState(){
    super.initState();
    context.read<SettingsViewModel>().checkDropBoxIsLogedIn();
  }

  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: SizedBox(
        height: 100.h,
        child: SingleChildScrollView(
          physics: const AlwaysScrollableScrollPhysics(),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                color: Theme.of(context).backgroundColor,
                padding: const EdgeInsets.all(2),
                child: SettingsList(
                  darkTheme: SettingsThemeData(
                    settingsListBackground: Theme.of(context).backgroundColor
                  ),
                  lightTheme: SettingsThemeData(
                      settingsListBackground: Theme.of(context).backgroundColor
                  ),
                  brightness: Brightness.dark,
                  physics: const NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  sections: [
                    SettingsSection(
                      title: TMText('general'.tr(), color: Theme.of(context).primaryColorDark),
                      tiles: <SettingsTile>[
                        SettingsTile.navigation(
                          leading: const Icon(Icons.language),
                          title: TMText('language'.tr()),
                          value: TMText('english'.tr()),
                          onPressed: (BuildContext context){
                            // showRadioSelect(context, ["English", "Persian"]);
                          },
                        ),
                        SettingsTile.navigation(
                          onPressed: (value) {

                          },
                          value: const TMText("System"),
                          leading: const Icon(Icons.dark_mode),
                          title: TMText('dark_mode'.tr()),
                        ),
                      ],
                    ),
                    SettingsSection(
                      title: TMText('BACKGROUND_SERVICES'.toLowerCase().tr(), color: Theme.of(context).primaryColorDark),
                      tiles: [
                        SettingsTile.switchTile(
                          onToggle: (value) {},
                          initialValue: true,
                          leading: const Icon(Icons.work_history),
                          title: TMText('Keep running app'.toLowerCase().replaceAll(" ", "_").tr()),
                          description: const TMText("Adding, Tagging & save images in database when app in background or closed"),
                        ),
                        SettingsTile.navigation(
                          value: const TMText("Request backup of images in device, to request lower size, encrypted of your images and non-loss of quality from safe cloud click here."),
                          leading: const Icon(Icons.backup),
                          title: TMText('Background backup'.toLowerCase().replaceAll(" ", "_").tr()),
                          description: TMText('Background backup description'.toLowerCase().replaceAll(" ", "_").tr()),
                        ),
                      ],
                    ),
                    SettingsSection(
                      title: TMText('BACKUP'.toLowerCase().tr(), color: Theme.of(context).primaryColorDark),
                      tiles: [
                        SettingsTile.navigation(
                          onPressed: (BuildContext context){
                            context.read<SettingsViewModel>().showBackupAccounts(context);
                          },
                          title: TMText('dropbox_account'.toLowerCase().tr()),
                          leading: const Icon(Icons.supervisor_account),
                          description: const TMText("Click here to view")
                        ),
                        SettingsTile.navigation(
                          title: TMText('default_service'.tr()),
                          leading: const Icon(Icons.miscellaneous_services),
                          description: const TMText('DropBox'),
                        ),
                        SettingsTile.switchTile(
                          title: TMText('auto_sync'.tr()),
                          leading: const Icon(Icons.sync),
                          description: TMText('Auto_sync_description'.tr()),
                          initialValue: false,
                          onToggle: (bool value) {

                          },
                        )
                      ],
                    ),
                    SettingsSection(
                      title: TMText('ADVANCE'.toLowerCase().tr(), color: Theme.of(context).primaryColorDark),
                      tiles: [
                        SettingsTile.navigation(
                          onPressed: (BuildContext context){
                          },
                          title: TMText('Processing method'.toLowerCase().replaceAll(" ", "_").tr()),
                          leading: const Icon(Icons.settings_input_svideo),
                          description: TMText('gpu'.tr()),
                        ),
                      ],
                    )
                  ],
                ),
              ),
              Container(
                height: 55,
                color: Theme.of(context).backgroundColor,
                padding: const EdgeInsets.only(bottom: 0, top: 0),
                child: InkWell(
                  hoverColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  splashColor: Colors.transparent,
                  focusColor: Colors.transparent,
                  onTap: (){
                    if (kDebugMode) {
                      print("Just this ...");
                      development = false ;
                    }
                  },
                  onDoubleTap: (){
                    development = true;
                  },
                  onLongPress: (){
                    if (development){
                      var snackBar = SnackBar(
                          content: const Text('Hi Ali!'),
                          action: SnackBarAction(label: "Developer options", onPressed: (){
                            showAbout(context);
                          })
                      );

                      ScaffoldMessenger.of(context).showSnackBar(snackBar);
                      development = false ;
                    }
                  },
                  child: const Center(
                      child: TMText("\"Smart gallery\" version 1.0.0")
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}


void showAbout(BuildContext context){
  showAboutDialog(
      context: context,
      applicationName: "Smart gallery",
      applicationVersion: "2.0.0",
      applicationIcon: Image.asset("assets/images/png/sensifai.png", width: 40, height: 40),

  );
}
