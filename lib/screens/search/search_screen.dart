import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:smartgallery/theme/widgets/tm_text.dart';
import 'package:smartgallery/utils/bar/app_bar.dart';

import '../../provider/notifiers/search/search_viewmodel.dart';
import '../gallery/image/image_screen.dart';

class SearchScreen extends StatefulWidget {
  final String? albumId ;
  final String? name ;
  const SearchScreen({this.name, this.albumId, Key? key}) : super(key: key);

  @override
  State<SearchScreen> createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {

  @override
  void initState(){
    super.initState();
    context.read<SearchViewModel>().initialSearchScreen(widget.albumId);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: widget.albumId != null ? getAppBar(
          context,
          title: TMText(widget.name.toString())
      ) : null,
      body: (SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 16.0, right: 16, top: 0),
              child: Container(
                height: 50,
                margin: const EdgeInsets.symmetric(vertical: 16),
                child: TextField(
                  keyboardType: TextInputType.name,
                  textInputAction: TextInputAction.search,
                  onSubmitted: (String query) async {
                    context.read<SearchViewModel>().search(context, query, first: true);
                  },
                  onChanged: (val) async {
                    
                  },
                  minLines: 1,
                  textCapitalization: TextCapitalization.sentences,
                  style: TextStyle(color: Theme.of(context).primaryColor),
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8),
                      ),
                      label: TMText('try_search_anything'.tr())
                  ),
                ),
              ),
            ),
            context.watch<SearchViewModel>().labels.isNotEmpty ? Container(
              padding: const EdgeInsets.only(left: 8, right: 8),
              width: 100.w,
              height: context.read<SearchViewModel>().album == null ? 78.h : 100.h,
              child: GridView.count(
                physics: const AlwaysScrollableScrollPhysics(),
                crossAxisCount: 3,
                shrinkWrap: false,
                controller: context.watch<SearchViewModel>().controller,
                children: List.generate(context.watch<SearchViewModel>().labels.length , (index){
                  var item = context.watch<SearchViewModel>().labels[index];
                  File file = File(item['path'].toString());
                  Uint8List? binary;
                  try {
                    binary = Uint8List.fromList(List<int>.from(json.decode(item['thumbnail'].toString())));
                  } catch (e){
                    if (kDebugMode){
                      print(e);
                    }
                  }
                  // String name = imageAdapter.fileName;
                  return Padding(
                    padding: const EdgeInsets.all(2.0),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(16),
                      child: Container(
                          width: double.infinity,
                          height: double.infinity,
                          color: Colors.grey[300],
                          child: InkWell(
                            highlightColor: Colors.transparent,
                            onTap: () async {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (builder) => ImageScreen(item, index, false)
                                  )
                              );
                            },
                            child: Hero(
                                tag: 'image_$index',
                                child: binary != null ? Image.memory(binary, fit: BoxFit.cover) : const Icon(Icons.broken_image_outlined)
                            ),
                          )
                      ),
                    ),
                  );
                }),
              ),
            ) :
            context.watch<SearchViewModel>().labels.isNotEmpty ? const SizedBox(width: 0, height: 0) : Padding(
              padding: EdgeInsets.only(top: 12.5.h),
              child: SizedBox(
                width: 150,
                height: 150,
                child: Lottie.asset("assets/animations/json/search.json"),
              ),
            ),
            context.watch<SearchViewModel>().labels.isNotEmpty ? const SizedBox(width: 0, height: 0) : Padding(
              padding: const EdgeInsets.only(top: 32),
              child: Center(
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    const Text("", style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold, letterSpacing: 1.5)),
                    context.read<SearchViewModel>().loading ? const TMText("Loading data ...\nPlease wait ...") : AnimatedTextKit(
                      isRepeatingAnimation: true,
                      repeatForever: true,
                      displayFullTextOnTap: true,
                      animatedTexts: [
                        FadeAnimatedText("Try search flower", textStyle: TextStyle(fontSize: 18, fontWeight: FontWeight.bold, letterSpacing: 1.5, color: Theme.of(context).primaryColor)),
                        FadeAnimatedText("Try search website", textStyle: TextStyle(fontSize: 18, fontWeight: FontWeight.bold, letterSpacing: 1.5, color: Theme.of(context).primaryColor)),
                        FadeAnimatedText("Try search cinema", textStyle: TextStyle(fontSize: 18, fontWeight: FontWeight.bold, letterSpacing: 1.5, color: Theme.of(context).primaryColor)),
                        FadeAnimatedText("Try search garden", textStyle: TextStyle(fontSize: 18, fontWeight: FontWeight.bold, letterSpacing: 1.5, color: Theme.of(context).primaryColor)),
                        FadeAnimatedText("Try search spider", textStyle: TextStyle(fontSize: 18, fontWeight: FontWeight.bold, letterSpacing: 1.5, color: Theme.of(context).primaryColor)),
                        FadeAnimatedText("Try search television", textStyle: TextStyle(fontSize: 18, fontWeight: FontWeight.bold, letterSpacing: 1.5, color: Theme.of(context).primaryColor))
                      ],
                      onTap: () {

                      },
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      )),
    );
  }
}
