import 'dart:convert';
import 'dart:io';

import 'package:dropbox_client/dropbox_client.dart';
import 'package:encryptor/encryptor.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:smartgallery/provider/notifiers/file_manager/file_manager_viewmodel.dart';
import 'package:smartgallery/theme/widgets/tm_text.dart';
import 'package:smartgallery/utils/bar/app_bar.dart';
import 'package:sn_progress_dialog/progress_dialog.dart';
import 'package:permission_handler/permission_handler.dart' as ph;
import 'package:url_launcher/url_launcher.dart';

import '../../theme/widgets/tm_icon.dart';
import '../../utils/notifications/alert.dart';

class FileManager extends StatefulWidget {
  const FileManager({Key? key}) : super(key: key);

  @override
  State<FileManager> createState() => _FileManagerState();
}

class _FileManagerState extends State<FileManager> {

  @override
  void initState(){
    super.initState();
    Future.delayed(const Duration(milliseconds: 500), (){
      context.read<FileManagerViewModel>().loadDirectory(context);
    });
  }


  Future<bool> _requestPermission(ph.Permission permission) async {
    return (await permission.request()).isGranted;
  }

  Future<bool> _hasAcceptedPermissions() async {
    if (Platform.isAndroid) {
      if (await _requestPermission(ph.Permission.storage) &&
          // access media location needed for android 10/Q
          await _requestPermission(ph.Permission.accessMediaLocation) &&
          // manage external storage needed for android 11/R
          await _requestPermission(ph.Permission.manageExternalStorage)) {
        return true;
      } else {
        return false;
      }
    }
    if (Platform.isIOS) {
      if (await _requestPermission(ph.Permission.photos)) {
        return true;
      } else {
        return false;
      }
    } else {
      // not android or ios
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: getAppBar(
          context,
          title: Image.asset("assets/images/png/dropbox_logo.png", height: 24)
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 16.0),
        child: ListView.builder(
          itemCount: context.watch<FileManagerViewModel>().result?.length != null ? context.watch<FileManagerViewModel>().result!.length + 1 : 1,
          itemBuilder: (context, index){
            if (index == 0){
              return Container(
                height: 45,
                width: 100.w,
                margin: const EdgeInsets.symmetric(horizontal: 16),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    color: Colors.orangeAccent
                ),
                child: InkWell(
                  onTap: () async {
                    bool accept = await _hasAcceptedPermissions();
                    if (!accept){
                      return;
                    }
                    var current = context.read<FileManagerViewModel>().path;
                    var parentSplit = current.split("/");
                    parentSplit.removeAt(parentSplit.length - 1);
                    String newPath = "";
                    for (int i = 0 ; i < parentSplit.length ; i++){
                      if (parentSplit[i].isNotEmpty){
                        newPath += "/${parentSplit[i]}";
                      }
                    }
                    print(newPath);
                    if (current.isEmpty){
                      Navigator.pop(context);
                    } else {
                      context.read<FileManagerViewModel>().loadDirectory(context, path: newPath);
                    }
                  },
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        const TMIcon(Icons.arrow_back_rounded, size: 24),
                        const SizedBox(width: 4),
                        SizedBox(
                          width: 75.w,
                          child: TMText("Back ${context.watch<FileManagerViewModel>().path}", fontWeight: FontWeight.bold, fontSize: 16, letterSpacing: 1.5, overflow: TextOverflow.ellipsis, softWrap: true, textWidthBasis: TextWidthBasis.parent)
                        )
                      ],
                    ),
                  ),
                ),
              );
            }
            // TMText(context.watch<FileManagerViewModel>().result![index - 1]["name"])
            return Container(
              height: 50,
              margin: const EdgeInsets.only(top: 8, left: 16, right: 16),
              padding: const EdgeInsets.only(left: 24, right: 24, top: 12, bottom: 12),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  color: const Color(0x88346EA0)
              ),
              child: InkWell(
                onLongPress: (){
                  var snackBar = SnackBar(
                    content: const Text('Do you want to download ?'),
                    action: SnackBarAction(
                      label: 'Download',
                      onPressed: () async {
                        Alert alert = Alert(context);
                        alert.error("Download Failed", "Only files can download to device", onTap: (){
                        });
                      },
                    ),
                  );
                  ScaffoldMessenger.of(context).showSnackBar(snackBar);
                },
                onTap: (){
                  if (context.read<FileManagerViewModel>().result![index - 1]["filesize"] != null){
                    var snackBar = SnackBar(
                      content: const Text('Do you want to download ?'),
                      action: SnackBarAction(
                        label: 'Download',
                        onPressed: () async {
                          var path = context.read<FileManagerViewModel>().result![index - 1]["pathDisplay"];
                          var name = context.read<FileManagerViewModel>().result![index - 1]["name"].toString();
                          if (path.toString().endsWith("sgei")){
                            var pathSpliter = path.toString().split("IF");
                            var ext = pathSpliter[1];
                            name.replaceAll("IF${ext}IF", "");
                            Directory? directory = (await getExternalStorageDirectory());
                            if (directory == null) return ;
                            var downloadPath = "${directory.path}/Smart-Gallery/";
                            var downloadDirectory = Directory(downloadPath);
                            if (!(await downloadDirectory.exists())){
                              downloadDirectory.create();
                            }
                            downloadPath = "$downloadPath${name.replaceAll("IF${ext}IF.sgei", "")}.$ext";
                            var downloadPercent = 0.0 ;
                            try {
                              ProgressDialog pd = ProgressDialog(context: context);
                              pd.show(max: 100, msg: "File Downloading");
                              final result = await Dropbox.download(path, downloadPath, (downloaded, total) async {
                                downloadPercent = ((downloaded * 100) / total);
                                pd.update(value: downloadPercent.toInt() ~/ 100);
                                if (downloaded == total){
                                  var encFile = File(downloadPath);
                                  var encStr = await encFile.readAsString();
                                  var key = 'EncryptKey0021';
                                  var encrypted = Encryptor.decrypt(key, encStr);
                                  var bytes = List<int>.from(json.decode(encrypted));
                                  await encFile.writeAsBytes(bytes);
                                  pd.close();
                                  downloadPercent = 0.0;
                                  LazyBox<String> paths = Hive.isBoxOpen("paths") ? Hive.lazyBox("paths") : await Hive.openLazyBox("paths");
                                  for (int i = 0 ; i < paths.length ; i++){
                                    var path = await paths.getAt(i);
                                    if (path == downloadPath){
                                      await paths.deleteAt(i);
                                    }
                                  }
                                  await paths.add(downloadPath);
                                  Alert alert = Alert(context);
                                  alert.success("File downloaded", "Click here to view $downloadPath", onTap: (){
                                    OpenFile.open(downloadPath);
                                  }, timeInMS: 5000);
                                }
                              });
                            } catch (e){
                              downloadPercent = 0.0 ;
                            }
                          } else {
                            Directory? directory = await getExternalStorageDirectory();
                            if (directory == null) return ;
                            var downloadPath = "${directory.path}/$name";
                            var downloadPercent = 0.0 ;
                            try {
                              ProgressDialog pd = ProgressDialog(context: context);
                              pd.show(max: 100, msg: "File Downloading");
                              final result = await Dropbox.download(path, downloadPath, (downloaded, total) async {
                                downloadPercent = ((downloaded * 100) / total);
                                pd.update(value: downloadPercent.toInt() ~/ 100);
                                if (downloaded == total){
                                  pd.close();
                                  downloadPercent = 0.0;
                                  LazyBox<String> paths = Hive.isBoxOpen("paths") ? Hive.lazyBox("paths") : await Hive.openLazyBox("paths");
                                  for (int i = 0 ; i < paths.length ; i++){
                                    var path = await paths.getAt(i);
                                    if (path == downloadPath){
                                      await paths.deleteAt(i);
                                    }
                                  }
                                  await paths.add(downloadPath);
                                  Alert alert = Alert(context);
                                  alert.success("File downloaded", "Click here to view $downloadPath", onTap: (){
                                    OpenFile.open(downloadPath);
                                  }, timeInMS: 5000);
                                }
                              });
                            } catch (e){
                              downloadPercent = 0.0 ;
                            }
                          }

                        },
                      ),
                    );
                    ScaffoldMessenger.of(context).showSnackBar(snackBar);
                  } else {
                    context.read<FileManagerViewModel>().loadDirectory(context, path: (context.read<FileManagerViewModel>().result![index - 1]["pathDisplay"]).toString());
                  }
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    context.watch<FileManagerViewModel>().result![index - 1]["filesize"] != null ? const TMIcon(Icons.file_present) : const TMIcon(Icons.folder),
                    const SizedBox(width: 8),
                    SizedBox(
                      width: 60.w,
                      child: TMText(context.watch<FileManagerViewModel>().result![index - 1]["name"], fontWeight: FontWeight.bold, fontSize: 16, letterSpacing: 1.5, overflow: TextOverflow.ellipsis, softWrap: true, textWidthBasis: TextWidthBasis.parent)
                    )
                  ],
                ),
              ),
            );
        }),
      ),
    );
  }
}
