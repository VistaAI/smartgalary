import 'dart:convert';
import 'dart:typed_data';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:smartgallery/provider/notifiers/likes/likes_viewmodel.dart';

import '../../theme/widgets/tm_text.dart';
import '../gallery/image/image_screen.dart';

class LikesScreen extends StatefulWidget {
  const LikesScreen({Key? key}) : super(key: key);

  @override
  State<LikesScreen> createState() => _LikesScreenState();
}

class _LikesScreenState extends State<LikesScreen> {

  @override
  void initState(){
    super.initState();
    context.read<LikesViewModel>().getImagesInAlbum(initial: true);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: context.watch<LikesViewModel>().loading ? SizedBox(
        width: 100.w,
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const CircularProgressIndicator(
              strokeWidth: 1.5,
            ),
            const SizedBox(
              height: 16,
            ),
            TMText('loading'.tr())
          ],
        ),
      ) : (context.watch<LikesViewModel>().images.isEmpty ? SizedBox(
        width: 100.w,
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            // SizedBox(height: 14.h),
            Lottie.asset("assets/animations/json/library_empty_state.json", width: 50.w, repeat: false),
            const TMText("Not found favorite photo's", fontWeight: FontWeight.bold, letterSpacing: 1.5, fontSize: 16),
            const SizedBox(height: 16),
          ],
        ),
      ) : GridView.count(
        // controller: context.watch<LikesViewModel>().controller,
        physics: const AlwaysScrollableScrollPhysics(),
        shrinkWrap: false,
        crossAxisCount: 3,
        mainAxisSpacing: 1.0,
        crossAxisSpacing: 1.0,
        children: List.generate(
            context.watch<LikesViewModel>().images.length,
                (index){
              var item = context.watch<LikesViewModel>().images[index] ;
              return Padding(
                padding: const EdgeInsets.all(2),
                child: Stack(
                  children: [
                    InkWell(
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(16),
                        child: Container(
                            width: double.infinity,
                            height: double.infinity,
                            color: Colors.grey[300],
                            child: Hero(
                              tag: 'image_$index',
                              child: FadeInImage(
                                placeholderFit: BoxFit.contain,
                                fit: BoxFit.cover,
                                placeholder: const Image(image: AssetImage("assets/images/png/sensifai.png"), fit: BoxFit.contain).image,
                                image: Image(image: MemoryImage(Uint8List.fromList(List<int>.from(json.decode(item['thumbnail'].toString())))), fit: BoxFit.cover).image,
                              ),
                            )
                          // Image.file(imageAlbums[index].file, fit: BoxFit.cover)
                        ),
                      ),
                      onTap: () async {
                        var newItem = Map<String, Object?>.from(item) ;
                        if (context.read<LikesViewModel>().changes.containsKey(index.toString())){
                          newItem['labels'] = context.read<LikesViewModel>().changes[index.toString()];
                        }
                        if (context.read<LikesViewModel>().likes.containsKey(index.toString())){
                          newItem['like'] = context.read<LikesViewModel>().likes[index.toString()];
                        }
                        Navigator.push(context, MaterialPageRoute(builder: (builder) => ImageScreen(item, index, true)));
                      },
                    ),
                  ],
                ),
              );
            }
        ),
      )),
    );
  }
}
