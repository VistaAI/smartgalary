import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:dropbox_client/dropbox_client.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:salomon_bottom_bar/salomon_bottom_bar.dart';
import 'package:sizer/sizer.dart';
import 'package:smartgallery/screens/downloads.dart';
import 'package:smartgallery/screens/file_manager/file_manager.dart';
import 'package:smartgallery/screens/search/search_screen.dart';
import 'package:smartgallery/screens/settings/settings_screen.dart';
import 'package:vision/vision.dart';

import '../provider/notifiers/navigation_viewmodel.dart';
import '../theme/widgets/tm_text.dart';
import '../utils/bar/app_bar.dart';
import '../utils/notifications/alert.dart';
import 'gallery/gallery_screen.dart';
import 'likes/likes_screen.dart';


class Navigation extends StatefulWidget {
  const Navigation({Key? key}) : super(key: key);

  @override
  State<Navigation> createState() => NavigationState();
}

class NavigationState extends State<Navigation> {

  static Vision vision = Vision();
  static bool initialed = false ;
  static bool tryCompleted = false ;


  static Future<List<String>> runModelOnImage(File? file) async {
    List<String> labels = [];
    if (!initialed){
      if (!tryCompleted){
        var initial = await vision.initial();
        initialed = initial;
        if (kDebugMode) {
          print("IN DEBUG MODE: $initialed");
        }
        tryCompleted = true ;
      } else {
        return labels;
      }
      return labels;
    }

    if (file == null) return labels;

    var bytes = await file.readAsBytes();
    String jsonLabels = await vision.runModelOnByteArray(Uint8List.fromList(bytes.toList()), 0.3);
    var mapLabeled = List<Map<String, dynamic>>.from(json.decode(jsonLabels));
    for (int i = 0 ; i < mapLabeled.length; i++){
      labels.add(mapLabeled[i]["description"].toString());
    }
    return labels;
  }



  @override
  void dispose() {
    context.read<NavigationViewModel>().controller.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    runModelOnImage(null);
    context.read<NavigationViewModel>().initialApp();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,
        appBar: getAppBar(
            context,
            actions: [
              IconButton(
                onPressed: () async {
                  Navigator.push(context, MaterialPageRoute(builder: (builder) => const Downloads()));
                },
                icon: const Icon(Icons.cloud_done),
                tooltip: "Downloads",
              ),
              IconButton(
                onPressed: () async {
                  await Dropbox.init("reg11w404ra1uh0", "reg11w404ra1uh0", "2ifyyj5siyv3qtf");
                  String? accessToken = await Dropbox.getAccessToken();
                  if (accessToken != null){
                    Navigator.push(context, MaterialPageRoute(builder: (builder) => const FileManager()));
                  } else {
                    Alert(context).error("Login Required", "Tap here to login to DropBox", timeInMS: 1500, onTap: () async {
                      await Dropbox.authorize();
                    });
                  }
                },
                icon: const Icon(Icons.folder),
                tooltip: "Download backups",
              ),
            ]
        ),
        bottomNavigationBar: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: SalomonBottomBar(
            itemPadding: EdgeInsets.symmetric(vertical: 0.9.h, horizontal: 5.w),
            currentIndex: context.watch<NavigationViewModel>().index,
            onTap: (i){
              context.read<NavigationViewModel>().changePage(i);
            },
            items: [
              /// Home
              SalomonBottomBarItem(
                activeIcon: const Icon(Icons.photo),
                icon: Icon(Icons.photo_outlined, color: Theme.of(context).primaryColor),
                title: TMText("gallery".tr(), fontWeight: FontWeight.w700, fontFamily: 'productSans', color: Colors.green),
                selectedColor: Colors.lightGreen,
              ),

              /// Search
              SalomonBottomBarItem(
                activeIcon: const Icon(Icons.search),
                icon: Icon(Icons.search, color: Theme.of(context).primaryColor),
                title: TMText("search".tr(), fontWeight: FontWeight.w700, fontFamily: 'productSans', color: Colors.green),
                selectedColor: Colors.lightGreen,
              ),

              /// Likes
              SalomonBottomBarItem(
                activeIcon: const Icon(Icons.favorite),
                icon: Icon(Icons.favorite_border, color: Theme.of(context).primaryColor),
                title: TMText("likes".tr(), fontWeight: FontWeight.w700, fontFamily: 'productSans', color: Colors.green),
                selectedColor: Colors.lightGreen,
              ),

              /// Profile
              SalomonBottomBarItem(
                activeIcon: const Icon(Icons.settings),
                icon: Icon(Icons.settings_outlined, color: Theme.of(context).primaryColor),
                title: TMText("settings".tr(), fontWeight: FontWeight.w700, fontFamily: 'productSans', color: Colors.green),
                selectedColor: Colors.lightGreen,
              ),
            ],
          ),
        ),
        body: context.watch<NavigationViewModel>().loading ? SizedBox(
          width: 100.w,
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const CircularProgressIndicator(
                strokeWidth: 1.5,
              ),
              const SizedBox(
                height: 16,
              ),
              TMText('fetching_albums'.tr())
            ],
          ),
        ) : PageView(
          scrollDirection: Axis.vertical,
          pageSnapping: true,
          physics: const NeverScrollableScrollPhysics(),
          controller: context.watch<NavigationViewModel>().controller,
          children: const [
            GalleryScreen(),
            SearchScreen(),
            LikesScreen(),
            SettingsScreen(),
          ],
        )
    );
  }
}
