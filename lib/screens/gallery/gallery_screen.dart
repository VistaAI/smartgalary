import 'dart:convert';
import 'dart:typed_data';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:photo_gallery/photo_gallery.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:smartgallery/provider/notifiers/gallery/gallery_viewmodel.dart';

import '../../theme/widgets/tm_text.dart';

class GalleryScreen extends StatefulWidget {
  const GalleryScreen({Key? key}) : super(key: key);

  @override
  State<GalleryScreen> createState() => _GalleryScreenState();
}

class _GalleryScreenState extends State<GalleryScreen> {

  @override
  void initState(){
    super.initState();
    context.read<GalleryViewModel>().initialGalleryScreen();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: context.watch<GalleryViewModel>().loading ? SizedBox(
        width: 100.w,
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const CircularProgressIndicator(
              strokeWidth: 1.5,
            ),
            const SizedBox(
              height: 16,
            ),
            TMText('fetching_albums'.tr()),
          ],
        ),
      ) : (
          context.watch<GalleryViewModel>().isEmpty ? SizedBox(
            width: 100.w,
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(height: 14.h),
                Lottie.asset("assets/animations/json/empty_albums.json", width: 100.w, height: 40.h),
                TMText('your_gallery_is_empty'.tr(), fontWeight: FontWeight.bold, letterSpacing: 1.5, fontSize: 16),
                const SizedBox(height: 16),
                TextButton(
                    onPressed: () async {
                      await context.read<GalleryViewModel>().initialGalleryScreen();
                    },
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: const [
                        Icon(Icons.refresh),
                        SizedBox(width: 4),
                        Text("Refresh", style: TextStyle(fontWeight: FontWeight.bold)),
                      ],
                    )
                )
              ],
            ),
          ) : SingleChildScrollView(
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 6, vertical: 16),
              child: GridView.count(
                physics: const NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                crossAxisCount: 3,
                mainAxisSpacing: 1.0,
                crossAxisSpacing: 1.0,
                children: List.generate(
                    context.watch<GalleryViewModel>().albums?.length ?? 0,
                    (index){
                      var item = context.read<GalleryViewModel>().albums?[index] ;
                      if (item == null) return Container();
                      return Padding(
                        padding: const EdgeInsets.all(2),
                        child: Stack(
                          children: [
                            ClipRRect(
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(16),
                                child: Container(
                                    width: double.infinity,
                                    height: double.infinity,
                                    color: Colors.grey[300],
                                    child: FadeInImage(
                                      placeholderFit: BoxFit.contain,
                                      fit: BoxFit.cover,
                                      placeholder: const Image(image: AssetImage("assets/images/png/sensifai.png"), fit: BoxFit.contain).image,
                                      image: Image(image: MemoryImage(Uint8List.fromList(List<int>.from(json.decode(context.watch<GalleryViewModel>().albums![index]['thumbnail'].toString())))), fit: BoxFit.cover).image,
                                    )
                                  // Image.file(imageAlbums[index].file, fit: BoxFit.cover)
                                ),
                              ),
                            ),
                            InkWell(
                              highlightColor: Colors.transparent,
                              borderRadius: BorderRadius.circular(16),
                              onTap: () async {
                                MediumType? mediumType = jsonToMediumType(item['medium_type'].toString());
                                String id = (item['album_id']).toString() ;
                                context.read<GalleryViewModel>().openAlbum(context, albumId: id, name: (item['name']).toString());
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(16),
                                    color: const Color(0x99000000)
                                ),
                                child: Center(
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(horizontal: 8.0),
                                      child: Text((item['name']).toString(), style: const TextStyle(color: Colors.white, fontWeight: FontWeight.w500, fontSize: 16, letterSpacing: 0.6), overflow: TextOverflow.ellipsis, softWrap: true, maxLines: 1, textAlign: TextAlign.center),
                                    )
                                ),
                              ),
                            )
                          ],
                        ),
                      );
                    }
                ),
              ),
            ),
          )
      )
    );
  }
}