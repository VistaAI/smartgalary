import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:achievement_view/achievement_view.dart';
import 'package:achievement_view/achievement_widget.dart';
import 'package:chips_choice_null_safety/chips_choice_null_safety.dart';
import 'package:dropbox_client/dropbox_client.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:encryptor/encryptor.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:like_button/like_button.dart';
import 'package:path_provider/path_provider.dart';
import 'package:photo_view/photo_view.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:smartgallery/provider/database/database.dart';
import 'package:smartgallery/provider/notifiers/gallery/album_viewmodel.dart';
import 'package:smartgallery/provider/notifiers/likes/likes_viewmodel.dart';
import 'package:sn_progress_dialog/progress_dialog.dart';
import 'package:string_validator/string_validator.dart';

import '../../../provider/notifiers/gallery/image_viewmodel.dart';
import '../../../theme/widgets/tm_icon.dart';
import '../../../theme/widgets/tm_text.dart';
import '../../../utils/bar/app_bar.dart';
import '../../../utils/notifications/alert.dart';
import '../../navigation_screen.dart';

class ImageScreen extends StatefulWidget {
  final Map<String, Object?> item ;
  final int index ;
  final bool likeScreen ;
  const ImageScreen(this.item, this.index, this.likeScreen, {Key? key}) : super(key: key);

  @override
  State<ImageScreen> createState() => _ImageScreenState();
}

class _ImageScreenState extends State<ImageScreen> {


  bool options = true ;

  @override
  ImageScreen get widget => super.widget;

  @override
  void initState() {
    context.read<ImageViewModel>().readQR(File(widget.item['path'].toString()));
    context.read<ImageViewModel>().setOptionsAndTags(List<String>.from(json.decode(widget.item['labels'].toString())));
    context.read<ImageViewModel>().download(File(widget.item['path'].toString()));
    super.initState();

  }

  void disableOptions(){
    setState((){
      options = false ;
    });
  }

  void enableOptions(){
    setState((){
      options = true ;
    });
  }

  bool isOptionsEnabled (){
    return options ;
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var item = widget.item ;
    var file = File(item['path'].toString());
    Future<bool> onLikeButtonTapped(bool isLiked) async{
      if (widget.likeScreen){
        if (!isLiked == true) {
          return isLiked;
        }
      }
      Databases.modify("UPDATE Images SET like = '${!isLiked}' WHERE path = '${file.path}' AND `id` = ${item['id']}");
      if (widget.likeScreen){
        context.read<LikesViewModel>().changeItem(context.read<ImageViewModel>().tags, widget.index, like: !isLiked);
      } else {
        context.read<AlbumViewModel>().changeItem(context.read<ImageViewModel>().tags, widget.index, like: !isLiked);
      }
      return !isLiked;
    }
    return Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,
        appBar: options ? getAppBar(
          context,
          // leading: IconButton(
          //     onPressed: () async {
          //       NavigationState.runModelOnImage(file, NavigationState.platform);
          //     },
          //     icon: const Icon(Icons.run_circle)
          // ),
          title: TMText(widget.item['name'].toString(), fontWeight: FontWeight.bold),
          actions: !context.watch<ImageViewModel>().functionRunned ? null : [
            context.watch<ImageViewModel>().imageDownloaded ? IconButton(
              onPressed: () async {
                Alert alert = Alert(context);
                await Dropbox.init("reg11w404ra1uh0", "reg11w404ra1uh0", "2ifyyj5siyv3qtf");
                String? token = await Dropbox.getAccessToken();
                if (token != null){
                  final filepath = file.path;
                  var split = file.path.split("/");
                  var dot = split[split.length - 1].split(".");
                  var upload = "${file.parent.path}/${dot[dot.length - 2]}IF${dot[dot.length - 1]}IF.sgei";
                  final result = await Dropbox.upload(filepath, upload, (uploaded, total) {
                    if (kDebugMode) {
                      print('progress $uploaded / $total');
                    }
                  });
                } else {
                  alert.error("Login Required", "Tap here to login to DropBox", timeInMS: 1500, onTap: () async {
                    await Dropbox.authorize();
                  });
                }
              },
              icon: const Icon(Icons.done),
              tooltip: "Image Uploaded with Encrypted format",
            ) : IconButton(
                onPressed: () async {
                  ProgressDialog pd = ProgressDialog(context: context);
                  pd.show(max: 100, msg: "File Uploading", progressType: ProgressType.normal, hideValue: true);
                  Alert alert = Alert(context);
                  await Dropbox.init("reg11w404ra1uh0", "reg11w404ra1uh0", "2ifyyj5siyv3qtf");
                  String? token = await Dropbox.getAccessToken();
                  if (token != null){
                    final filepath = file.path;
                    var split = file.path.split("/");
                    var dot = split[split.length - 1].split(".");
                    var upload = "${file.parent.path}/${dot[dot.length - 2]}IF${dot[dot.length - 1]}IF.sgei";

                    final directory = await getApplicationDocumentsDirectory();
                    var plainText = json.encode((await file.readAsBytes()).toList());
                    var key = 'EncryptKey0021';

                    var encrypted = Encryptor.encrypt(key, plainText);
                    var uploadFile = File("${directory.path}${dot[dot.length - 2]}IF${dot[dot.length - 1]}IF.sgei");
                    var fileForUpload = await uploadFile.writeAsString(encrypted);

                    try {
                      final result = await Dropbox.upload(fileForUpload.path, upload, (uploaded, total) {
                        if (uploaded == total){
                          context.read<ImageViewModel>().changeUploadedStatus(true);
                        }
                      });
                    } catch (e){
                      context.read<ImageViewModel>().changeUploadedStatus(false);
                    }
                  } else {
                    alert.error("Login Required", "Tap here to login to DropBox", timeInMS: 1500, onTap: () async {
                      await Dropbox.authorize();
                    });
                  }
                  pd.close();
                },
                icon: const Icon(Icons.cloud_upload_outlined),
                tooltip: "Upload Encrypted image to dropbox cloud",
            )
          ]
        ) : null,
        resizeToAvoidBottomInset: false,
        bottomSheet: options ? Container(
          height: 70,
          color: Theme.of(context).backgroundColor,
          child: Center(
            child: Padding(
              padding: const EdgeInsets.only(bottom: 8.0, left: 8, right: 8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: [
                  Expanded(
                    flex: 1,
                    child: TextButton(
                      onPressed: (){
                        context.read<ImageViewModel>().
                        shareImage(
                            file,
                            item['name'].toString()
                        );
                      },
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          const TMIcon(Icons.share_outlined),
                          const SizedBox(
                            height: 4,
                          ),
                          TMText("share".tr())
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: TextButton(
                      onPressed: () async {
                        // var isLiked = widget.dbImage?.like;
                        // if (isLiked == null) return ;
                        onLikeButtonTapped(toBoolean(widget.item['like'].toString()));
                        setState((){
                          widget.item['like'] = !toBoolean(widget.item['like'].toString());
                        });
                      },
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          LikeButton(
                            size: 24,
                            circleColor:
                            const CircleColor(start: Colors.deepPurpleAccent, end: Colors.red),
                            isLiked: toBoolean(item['like'].toString()),
                            bubblesColor: const BubblesColor(
                              dotPrimaryColor: Colors.red,
                              dotSecondaryColor: Colors.deepPurpleAccent,
                            ),
                            likeBuilder: (bool isLiked) {
                              return TMIcon(
                                isLiked ? Icons.favorite_outlined : Icons.favorite_outline,
                                color: isLiked ? Colors.red : Theme.of(context).primaryColor,
                                size: 24,
                              );
                            },
                            onTap: onLikeButtonTapped,
                          ),
                          const SizedBox(
                            height: 4,
                          ),
                          TMText("like".tr())
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: TextButton(
                      onPressed: (){
                        context.read<ImageViewModel>().
                        editImage(
                            file,
                            context
                        );
                      },
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          const TMIcon(Icons.edit_outlined),
                          const SizedBox(
                            height: 4,
                          ),
                          TMText("edit".tr())
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: TextButton(
                      onPressed: () async {



                        showLabelChips(context, item, widget.index, widget.likeScreen);
                      },
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          const TMIcon(Icons.label_outline),
                          const SizedBox(
                            height: 4,
                          ),
                          TMText("labels".tr())
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: TextButton(
                      onPressed: () async {
                        // var file = widget.file;
                        context.read<AlbumViewModel>().deleteFile(context, file, widget.index);
                        Navigator.pop(context);
                      },
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          const TMIcon(Icons.delete_outline),
                          const SizedBox(
                            height: 4,
                          ),
                          TMText("delete".tr())
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ) : null,
        body: Stack(
          children: [
            Center(
              child: Hero(
                tag: 'image_${widget.index}',
                child: PhotoView(
                  minScale: PhotoViewComputedScale.contained * 1.0,
                  initialScale: PhotoViewComputedScale.contained * 1.0,
                  maxScale: PhotoViewComputedScale.contained * 5.0,
                  wantKeepAlive: true,
                  imageProvider: FileImage(file),
                  onTapUp: (BuildContext context, TapUpDetails details, PhotoViewControllerValue controllerValue){
                    if (isOptionsEnabled()){
                      disableOptions();
                    } else {
                      enableOptions();
                    }
                  },
                  backgroundDecoration: const BoxDecoration(
                      color: Colors.transparent
                  ),
                ),
              ),
            ),
            options ? Positioned(
                top: 70.h,
                child: Container(
                  width: 100.w,
                  decoration: const BoxDecoration(
                    // color: Colors.blueGrey,
                    // borderRadius: BorderRadius.all(
                    //   Radius.circular(16)
                    // )
                  ),
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  child: Center(
                    child: context.watch<ImageViewModel>().qrTitle != null ? AnimatedContainer(
                      duration: const Duration(seconds: 4),
                      child: ElevatedButton(
                          style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all(Colors.blueGrey)
                          ),
                          onPressed: (){
                            context.read<ImageViewModel>().launchURL(context.read<ImageViewModel>().qrTitle, (context.read<ImageViewModel>().QRCodeResult?.message).toString());
                          },
                          child: Text(context.watch<ImageViewModel>().qrTitle.toString())
                      ),
                    ) : const SizedBox(width: 0, height: 0),
                  ),
                )) : const SizedBox(width: 0, height: 0)
          ],
        )
    );
  }
}


void showLabelChips(BuildContext context, Map<String, Object?> item, int index, bool likeScreen) async {
  // context.read<NavigationViewModel>().(dbImage.labels);
  // assets/ai/models/sensifai/converted_model.tflite
  // assets/ai/models/sensifai/classes-trainable.txt
  // var images = await db?.query("Images", where: "image_id = '${dbImage.id}'");
  // if (images == null) return ;
  // dbImage.labels = List<String>.from(json.decode(images[0]['labels'].toString()));
  // context.read<ImageViewModel>().setOptionsAndTags(dbImage.labels);
  var file = File(item['path'].toString());
  String value = "";
  showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      backgroundColor: Theme.of(context).backgroundColor,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topRight: Radius.circular(16),
            topLeft: Radius.circular(16)
        ),
      ),
      builder: (builder){
        return StatefulBuilder(
            builder: (context, setState) {
              return Padding(
                padding: EdgeInsets.only(
                    top: 16,
                    left: 16,
                    right: 16,
                    bottom: MediaQuery.of(context).viewInsets.bottom + 16
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    const TMText("Labels", fontWeight: FontWeight.w600, fontSize: 18, letterSpacing: 1.5),
                    const SizedBox(
                      height: 12,
                    ),
                    Container(
                      margin: const EdgeInsets.symmetric(horizontal: 8),
                      height: 55,
                      child: TextField(
                        textInputAction: TextInputAction.done,
                        onSubmitted: (String value) async {
                          if ( value.isEmpty ) return;
                          if (!context.read<ImageViewModel>().options.contains(value)){
                            if (context.read<ImageViewModel>().options == context.read<ImageViewModel>().tags){
                              context.read<ImageViewModel>().options.add(value);
                            } else {
                              context.read<ImageViewModel>().options.add(value);
                              context.read<ImageViewModel>().tags.add(value);
                            }

                            await Databases.db?.update("Images",  {
                              "labels": json.encode(context.read<ImageViewModel>().tags)
                            }, where: "album_id = '${item['album_id']}' AND image_id = '${item['image_id']}'");

                            if (likeScreen){
                              await context.read<LikesViewModel>().changeItem(context.read<ImageViewModel>().tags, index);
                            } else {
                              await context.read<AlbumViewModel>().changeItem(context.read<ImageViewModel>().tags, index);
                            }
                            AchievementView(
                              context,
                              title: "Added successfully",
                              subTitle: "\"$value\" word added",
                              //onTab: _onTabAchievement,
                              icon: const Icon(Icons.done, color: Colors.white),
                              typeAnimationContent: AnimationTypeAchievement.fadeSlideToUp,
                              borderRadius: BorderRadius.circular(8),
                              color: Colors.blueGrey,
                              textStyleTitle: const TextStyle(),
                              textStyleSubTitle: const TextStyle(),
                              alignment: Alignment.topCenter,
                              duration: const Duration(milliseconds: 1500),
                              isCircle: true,
                            ).show();
                          } else {
                            AchievementView(
                                context,
                                title: "Already added",
                                subTitle: "Try with other words",
                                icon: const Icon(Icons.close, color: Colors.white),
                                typeAnimationContent: AnimationTypeAchievement.fadeSlideToUp,
                                borderRadius: BorderRadius.circular(360),
                                color: Colors.redAccent,
                                textStyleTitle: const TextStyle(),
                                textStyleSubTitle: const TextStyle(),
                                alignment: Alignment.topCenter,
                                duration: const Duration(milliseconds: 1500),
                                isCircle: true
                            ).show();
                          }
                          setState((){});
                        },
                        onChanged: (text){
                          value = text ;
                        },
                        style: TextStyle(fontSize: 18, fontWeight: FontWeight.w400, color: Theme.of(context).primaryColor),
                        cursorColor: Theme.of(context).primaryColor,
                        decoration: InputDecoration(
                            fillColor: Theme.of(context).primaryColor,
                            border: OutlineInputBorder(
                              borderSide: const BorderSide(
                                  color: Colors.green,
                                  width: 1.0,
                                  style: BorderStyle.solid
                              ),
                              borderRadius: BorderRadius.circular(8),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: Theme.of(context).primaryColor,
                                  width: 1.0,
                                  style: BorderStyle.solid
                              ),
                              borderRadius: BorderRadius.circular(8),
                            ),
                            prefixIcon: Icon(Icons.label_outline, color: Theme.of(context).primaryColor),
                            hintText: "New label",
                            hintStyle: TextStyle(fontSize: 16, color: Theme.of(context).primaryColor),
                            suffixIcon: IconButton(
                              icon: const Icon(Icons.done, color: Colors.blueAccent),
                              onPressed: () async {
                                if ( value.isEmpty ) return;
                                if (!context.read<ImageViewModel>().options.contains(value)){
                                  if (context.read<ImageViewModel>().options == context.read<ImageViewModel>().tags){
                                    context.read<ImageViewModel>().options.add(value);
                                  } else {
                                    context.read<ImageViewModel>().options.add(value);
                                    context.read<ImageViewModel>().tags.add(value);
                                  }

                                  await Databases.db?.update("Images",  {
                                    "labels": json.encode(context.read<ImageViewModel>().tags)
                                  }, where: "album_id = '${item['album_id']}' AND image_id = '${item['image_id']}'");

                                  if (likeScreen){
                                    await context.read<LikesViewModel>().changeItem(context.read<ImageViewModel>().tags, index);
                                  } else {
                                    await context.read<AlbumViewModel>().changeItem(context.read<ImageViewModel>().tags, index);
                                  }

                                  AchievementView(
                                      context,
                                      title: "Added successfully",
                                      subTitle: "\"$value\" word added",
                                      //onTab: _onTabAchievement,
                                      icon: const Icon(Icons.done, color: Colors.white),
                                      typeAnimationContent: AnimationTypeAchievement.fadeSlideToUp,
                                      borderRadius: BorderRadius.circular(8),
                                      color: Colors.blueGrey,
                                      textStyleTitle: const TextStyle(),
                                      textStyleSubTitle: const TextStyle(),
                                      alignment: Alignment.topCenter,
                                      duration: const Duration(milliseconds: 1500),
                                      isCircle: true
                                  ).show();
                                } else {
                                  AchievementView(
                                      context,
                                      title: "Already added",
                                      subTitle: "Try with other words",
                                      icon: const Icon(Icons.close, color: Colors.white),
                                      typeAnimationContent: AnimationTypeAchievement.fadeSlideToUp,
                                      borderRadius: BorderRadius.circular(8),
                                      color: Colors.redAccent,
                                      textStyleTitle: const TextStyle(),
                                      textStyleSubTitle: const TextStyle(),
                                      alignment: Alignment.topCenter,
                                      duration: const Duration(milliseconds: 1500),
                                      isCircle: true
                                  ).show();
                                }
                                setState((){});
                              },
                            )
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 4,
                    ),
                    ChipsChoice<String>.multiple(
                      value: context.watch<ImageViewModel>().tags,
                      spinnerColor: Colors.green,
                      choiceActiveStyle: C2ChoiceStyle(
                        labelStyle: const TextStyle(color: Colors.green),
                        // backgroundColor: Colors.green,
                        borderColor: Colors.green,
                        color: Colors.green,
                        brightness: MediaQueryData.fromWindow(WidgetsBinding.instance.window).platformBrightness,
                        disabledColor: Colors.black,
                        // disabledColor: Colors.black,
                      ),
                      choiceStyle: C2ChoiceStyle(
                        labelStyle: const TextStyle(color: Colors.green),
                        // backgroundColor: Colors.green,
                        borderColor: Colors.green,
                        borderWidth: 0.5,
                        color: Colors.green,
                        brightness: MediaQueryData.fromWindow(WidgetsBinding.instance.window).platformBrightness,
                        disabledColor: Colors.black,
                        // disabledColor: Colors.black,
                      ),
                      onChanged: (val) async {
                        setState((){
                          context.read<ImageViewModel>().valueChanged(val);
                        });
                        await Databases.db?.update("Images",  {
                          "labels": json.encode(val)
                        }, where: "album_id = '${item['album_id']}' AND image_id = '${item['image_id']}'");

                        if (likeScreen){
                          await context.read<LikesViewModel>().changeItem(context.read<ImageViewModel>().tags, index);
                        } else {
                          await context.read<AlbumViewModel>().changeItem(context.read<ImageViewModel>().tags, index);
                        }

                        AchievementView(
                            context,
                            title: "Labels changed!",
                            subTitle: "We are only notified to you.",
                            icon: const Icon(Icons.info, color: Colors.white),
                            typeAnimationContent: AnimationTypeAchievement.fadeSlideToUp,
                            borderRadius: BorderRadius.circular(360),
                            color: Colors.blueAccent,
                            textStyleTitle: const TextStyle(),
                            textStyleSubTitle: const TextStyle(),
                            alignment: Alignment.topCenter,
                            duration: const Duration(milliseconds: 1500),
                            isCircle: true
                        ).show();
                      },
                      placeholder: "No items found",
                      placeholderStyle: TextStyle(color: Theme.of(context).primaryColor),
                      choiceItems: C2Choice.listFrom<String, String>(
                        source: context.watch<ImageViewModel>().options,
                        value: (i, v) => v,
                        label: (i, v) => v,
                      ),
                    )
                  ],
                ),
              );
            }
        );
      }
  );
}

