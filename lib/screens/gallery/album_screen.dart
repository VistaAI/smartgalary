import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:smartgallery/provider/database/database.dart';
import 'package:smartgallery/screens/gallery/image/image_screen.dart';
import 'package:smartgallery/screens/search/search_screen.dart';
import 'package:smartgallery/theme/widgets/tm_text.dart';
import 'package:smartgallery/utils/artificial_intelligence/classifier_float.dart';
import 'package:smartgallery/utils/artificial_intelligence/classifier_quant.dart';
import 'package:smartgallery/utils/bar/app_bar.dart';
// import 'package:tflite_flutter_helper/tflite_flutter_helper.dart';

import '../../provider/notifiers/gallery/album_viewmodel.dart';
import '../../utils/artificial_intelligence/classifier.dart';
import '../../utils/artificial_intelligence/photo_tagging.dart';
import 'package:image/image.dart' as img;
import 'dart:developer' as dev;

class AlbumScreen extends StatefulWidget {
  final String? albumId;
  final String? title;
  const AlbumScreen({this.albumId, this.title, Key? key}) : super(key: key);

  @override
  State<AlbumScreen> createState() => _AlbumScreenState();
}

class _AlbumScreenState extends State<AlbumScreen> {

  @override
  void initState(){
    super.initState();
    context.read<AlbumViewModel>().getImagesInAlbum(albumId: widget.albumId, initial: true);
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      bottomSheet: context.read<AlbumViewModel>().loading == false ? null : Container(
        width: 100.w,
        height: 45,
        color: Colors.white,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: const [
            SizedBox(
              width: 20,
              height: 20,
              child: CircularProgressIndicator(
                strokeWidth: 2.0
              ),
            ),
            SizedBox(
              width: 16,
            ),
            TMText("Loading...", fontWeight: FontWeight.bold)
          ],
        ),
      ),
      appBar: getAppBar(
        context,
        title: TMText(widget.title ?? "Album", fontWeight: FontWeight.bold, fontSize: 18, letterSpacing: 1.5),
        actions: [
          IconButton(onPressed: (){
            Navigator.push(context, MaterialPageRoute(builder: (builder) => SearchScreen(albumId: widget.albumId, name: widget.title)));
          }, icon: const Icon(Icons.search))
        ]
      ),
      body: context.watch<AlbumViewModel>().images.isEmpty ? SizedBox(
        width: 100.w,
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(height: 14.h),
            Lottie.asset("assets/animations/json/empty_albums.json", width: 100.w, height: 40.h),
            const TMText("UNDER TAGGING ...", fontWeight: FontWeight.bold, letterSpacing: 1.5, fontSize: 16),
            const SizedBox(height: 16),
          ],
        ),
      ) : GridView.count(
        controller: context.watch<AlbumViewModel>().controller,
        shrinkWrap: true,
        crossAxisCount: 3,
        mainAxisSpacing: 1.0,
        crossAxisSpacing: 1.0,
        children: List.generate(
            context.watch<AlbumViewModel>().images.length,
                (index){
              var item = context.watch<AlbumViewModel>().images[index] ;
              Uint8List? ul;
              ImageProvider<Object> image = const AssetImage("assets/images/png/sensifai.png");
              try {
                ul = Uint8List.fromList(List<int>.from(json.decode(item['thumbnail'].toString())));
              } catch (e){
                ul = null;
                if (kDebugMode){
                  print(e);
                }
              }
              if (ul != null){
                image = MemoryImage(ul);
              } else {
                image = FileImage(File(item['path'].toString()));
              }
              return Padding(
                padding: const EdgeInsets.all(2),
                child: Stack(
                  children: [
                    InkWell(
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(16),
                        child: Container(
                            width: double.infinity,
                            height: double.infinity,
                            color: Colors.grey[300],
                            child: Hero(
                              tag: 'image_$index',
                              child: FadeInImage(
                                placeholderFit: BoxFit.contain,
                                fit: BoxFit.cover,
                                placeholder: const Image(image: AssetImage("assets/images/png/sensifai.png"), fit: BoxFit.contain).image,
                                image: Image(image: image, fit: BoxFit.cover).image,
                              ),
                            )
                          // Image.file(imageAlbums[index].file, fit: BoxFit.cover)
                        ),
                      ),
                      onTap: () async {
                        var newItem = Map<String, Object?>.from(item) ;
                        if (context.read<AlbumViewModel>().changes.containsKey(index.toString())){
                          newItem['labels'] = context.read<AlbumViewModel>().changes[index.toString()];
                        }
                        if (context.read<AlbumViewModel>().likes.containsKey(index.toString())){
                          newItem['like'] = context.read<AlbumViewModel>().likes[index.toString()];
                        }
                        Navigator.push(context, MaterialPageRoute(builder: (builder) => ImageScreen(item, index, false)));
                      },
                    ),
                  ],
                ),
              );
            }
        ),
      ),
    );
  }
}
