import 'dart:convert';

import 'package:photo_gallery/photo_gallery.dart';

import '../../provider/database/database.dart';

class Gallery {
  static Future<List<Album>> getAlbumsFromDevice ({bool withAll = false}) async {
    var list = await PhotoGallery.listAlbums(
      mediumType: MediumType.image,
    );
    if (!withAll){
      list.removeAt(0);
    }
    return list ;
  }
  static Future<List<Map<String, Object?>>?> getAlbums ({mainThread = true}) async {
    return await Databases.getAlbums(mainThread: mainThread);
  }

  static Future<Album> getAlbum (String id) async {
    var album =  await Databases.getAlbum(id);
    return Album.fromJson(album[0]);
  }

  static Future<int?> addNewAlbumToApp (String values) async {
    return await Databases.modify("INSERT INTO Albums (name, isAllAlbum, album_id, thumbnail, medium_type, count) VALUES ($values);");
  }


  static Future<List<Map<String, Object?>>?> getFavoriteImages (int limit, int offset) async {
    String query = "SELECT * FROM Images WHERE like = 'true' ORDER BY `id` ASC LIMIT $limit OFFSET $offset ;";
    return await Databases.runQuery(query);
  }

  static Future<List<Map<String, Object?>>?> getImagesInAlbum (int limit, int offset, {String? albumId}) async {
    String query = "SELECT * FROM Images ";
    if (albumId == "__ALL__"){
      query += "ORDER BY `id` ASC LIMIT $limit OFFSET $offset";
    } else {
      query += "WHERE album_id = '$albumId' ORDER BY `id` ASC LIMIT $limit OFFSET $offset";
    }
    query += " ;";
    return await Databases.runQuery(query);
  }
}