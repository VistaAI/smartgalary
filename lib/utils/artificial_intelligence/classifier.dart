// import 'dart:collection';
// import 'dart:math';
//
// import 'package:flutter/foundation.dart' as f;
// import 'package:flutter/foundation.dart';
// import 'package:image/image.dart';
// import 'package:collection/collection.dart';
// import 'package:tflite_flutter/tflite_flutter.dart';
// import 'package:tflite_flutter_helper/tflite_flutter_helper.dart';
//
// abstract class Classifier {
//   late Interpreter interpreter;
//   late InterpreterOptions _interpreterOptions;
//
//   late List<int> _inputShape;
//   late List<int> _outputShape;
//
//   late TensorImage _inputImage;
//   late TensorBuffer _outputBuffer;
//
//   late TfLiteType _inputType;
//   late TfLiteType _outputType;
//
//   final String _labelsFileName = 'assets/labels.txt';
//
//   final int _labelsLength = 5000;
//
//   late var _probabilityProcessor;
//
//   late List<String> labels;
//
//   String get modelName;
//
//   NormalizeOp get preProcessNormalizeOp;
//   NormalizeOp get postProcessNormalizeOp;
//
//   Classifier({int? numThreads}) {
//     _interpreterOptions = InterpreterOptions();
//
//
//     if (numThreads != null) {
//       _interpreterOptions.threads = numThreads;
//     }
//
//     loadModel();
//     loadLabels();
//   }
//
//   Future<void> loadModel() async {
//     try {
//       interpreter =
//       await Interpreter.fromAsset(modelName, options: _interpreterOptions);
//       print('Interpreter Created Successfully');
//
//       _inputShape = interpreter.getInputTensor(0).shape;
//       _outputShape = interpreter.getOutputTensor(0).shape;
//       _inputType = interpreter.getInputTensor(0).type;
//       _outputType = interpreter.getOutputTensor(0).type;
//
//       _outputBuffer = TensorBuffer.createFixedSize(_outputShape, _outputType);
//       _probabilityProcessor =
//           TensorProcessorBuilder().add(postProcessNormalizeOp).build();
//     } catch (e) {
//       print('Unable to create interpreter, Caught Exception: ${e.toString()}');
//     }
//   }
//
//   Future<void> loadLabels() async {
//     try {
//       labels = await FileUtil.loadLabels(_labelsFileName);
//       if (kDebugMode) {
//         print('Labels loaded successfully');
//       }
//     } catch (e) {
//       if (kDebugMode){
//         print('Unable to load labels');
//       }
//     }
//   }
//
//   TensorImage _preProcess() {
//     int cropSize = min(_inputImage.height, _inputImage.width);
//     return ImageProcessorBuilder()
//         .add(ResizeWithCropOrPadOp(cropSize, cropSize))
//         .add(ResizeOp(
//         _inputShape[1], _inputShape[2], ResizeMethod.NEAREST_NEIGHBOUR))
//         .add(preProcessNormalizeOp)
//         .build()
//         .process(_inputImage);
//   }
//
//   Map<String, double> predict(Image image) {
//     final pres = DateTime.now().millisecondsSinceEpoch;
//     _inputImage = TensorImage(_inputType);
//     _inputImage.loadImage(image);
//     _inputImage = _preProcess();
//     final pre = DateTime.now().millisecondsSinceEpoch - pres;
//
//     print('Time to load image: $pre ms');
//
//     final runs = DateTime.now().millisecondsSinceEpoch;
//     interpreter.run(_inputImage.buffer, _outputBuffer.getBuffer());
//     final run = DateTime.now().millisecondsSinceEpoch - runs;
//
//     print('Time to run inference: $run ms');
//
//     Map<String, double> labeledProb = TensorLabel.fromList(
//         labels, _probabilityProcessor.process(_outputBuffer))
//         .getMapWithFloatValue();
//
//     final sortedValues =
//     labeledProb.entries.toList()
//       ..sort((key1, key2) =>
//           key2.value.compareTo(key1.value)
//       );
//     labeledProb = Map.fromEntries(sortedValues);
//
//     Map<String, double> finalMap = {};
//     for (int i = 0 ; i < labeledProb.length ; i++){
//       var key = labeledProb.keys.toList()[i];
//       var value = labeledProb[key];
//       if (value != null){
//         if (value != 0.0){
//           if (finalMap.length < 6){
//             if (!key.contains(",")){
//               if (!finalMap.containsKey(key)){
//                 finalMap[key.toLowerCase()] = value;
//               }
//             } else {
//               List<String> splinted = key.split(",");
//               for (int j = 0 ; j < splinted.length ; j++){
//                 String newKey = j != 0 ? splinted[j].substring(1) : splinted[j];
//                 if (!finalMap.containsKey(newKey)){
//                   finalMap[newKey.toLowerCase()] = value;
//                 }
//               }
//             }
//           }
//         }
//       }
//     }
//
//     return finalMap;
//   }
//
//
//   Map<String, double> getMapByPredict(Image image) {
//     final pres = DateTime.now().millisecondsSinceEpoch;
//     _inputImage = TensorImage(_inputType);
//     _inputImage.loadImage(image);
//     _inputImage = _preProcess();
//     final pre = DateTime.now().millisecondsSinceEpoch - pres;
//
//     print('Time to load image: $pre ms');
//
//     final runs = DateTime.now().millisecondsSinceEpoch;
//     interpreter.run(_inputImage.buffer, _outputBuffer.getBuffer());
//     final run = DateTime.now().millisecondsSinceEpoch - runs;
//
//     print('Time to run inference: $run ms');
//
//     Map<String, double> labeledProb = TensorLabel.fromList(
//         labels, _probabilityProcessor.process(_outputBuffer))
//         .getMapWithFloatValue();
//
//     final sortedValues =
//     labeledProb.entries.toList()
//       ..sort((key1, key2) => key1.value.compareTo(key2.value)
//       );
//     labeledProb = Map.fromEntries(sortedValues);
//
//     return labeledProb ;
//   }
//
//   void close() {
//     interpreter.close();
//   }
// }
//
// PriorityQueue<MapEntry<String, double>> getTopProbability(Map<String, double> labeledProb) {
//   var pq = PriorityQueue<MapEntry<String, double>>(compare);
//   pq.addAll(labeledProb.entries);
//
//   return pq;
// }
//
// int compare(MapEntry<String, double> e1, MapEntry<String, double> e2) {
//   if (e1.value > e2.value) {
//     return -1;
//   } else if (e1.value == e2.value) {
//     return 0;
//   } else {
//     return 1;
//   }
// }