import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';

import '../../theme/widgets/tm_icon.dart';
import '../../theme/widgets/tm_text.dart';
import '../permissions/permission_manager.dart';

class Notifications {
  AwesomeNotifications? notifications ;

  Future<AwesomeNotifications?> initial () async {
    notifications = AwesomeNotifications();
    if (notifications == null) return null;
    notifications?.initialize(
      // set the icon to null if you want to use the default app icon
        null,
        [
          NotificationChannel(
              channelGroupKey: 'success_channel_group',
              channelKey: 'success_channel',
              channelName: 'Success Notifications',
              channelDescription: 'Notification channel for success events',
              defaultColor: const Color(0xFF9D50DD),
              ledColor: Colors.white)
        ],
        // Channel groups are only visual and are not required
        channelGroups: [
          NotificationChannelGroup(
              channelGroupkey: 'success_channel_group',
              channelGroupName: 'Success group'
          )
        ],
        debug: false
    );


    return notifications ;
  }



  Future<void> sendNotification(Map<String, dynamic> data) async {
    notifications?.createNotification(
        content: NotificationContent(
            id: 1,
            channelKey: data['channelKey'],
            title: data['title'].toString().tr(),
            body: data['body'].toString().tr(),
            wakeUpScreen: true,
        )
    );
  }


  Future<bool> checkAllowNotifications(BuildContext context) async {
    bool allow = false ;
    notifications?.isNotificationAllowed().then((isAllowed) async {
      if (!isAllowed) {
        await showRequestNotificationPermissionDialog(
            context,
            notifications,
            onGranted: (){
              allow = true ;
            },
            onDenied: (){
              allow = false ;
            }
        );
      }
    });
    return allow ;
  }

  Future<void> showRequestNotificationPermissionDialog ( BuildContext context, AwesomeNotifications? notifications, {Function()? onGranted, Function()? onDenied}) async {
    await showModalBottomSheet(
        context: context,
        backgroundColor: Theme.of(context).backgroundColor,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(topLeft: Radius.circular(16), topRight: Radius.circular(16)),
        ),
        builder: (builder){
          return Padding(
            padding: const EdgeInsets.all(24),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    const Padding(
                      padding: EdgeInsets.symmetric(horizontal: 16.0),
                      child: TMIcon(Icons.notifications),
                    ),
                    TMText('notifications_disabled'.tr(), fontWeight: FontWeight.bold, fontSize: 18)
                  ],
                ),
                Container(
                  margin: const EdgeInsets.only(left: 16, right: 16, top: 8),
                  child: TMText('please_allow_notification_messages'.tr(), textAlign: TextAlign.justify, fontSize: 14, height: 2),
                ),
                const SizedBox(
                  height: 16,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                      child: SizedBox(
                        height: 40,
                        child: ElevatedButton(
                          onPressed: () async {
                            await notifications?.requestPermissionToSendNotifications();
                            bool? isAllowed = await notifications?.isNotificationAllowed();
                            if (isAllowed == true){
                              onGranted?.call();
                            } else {
                              onDenied?.call();
                            }
                            Navigator.of(builder).pop();
                          },
                          child: Text('yes'.tr()),
                        ),
                      ),
                    ),
                    const SizedBox(
                      width: 8,
                    ),
                    Expanded(
                      child:  SizedBox(
                        height: 40,
                        child: TextButton(
                          onPressed: (){
                            if(onDenied != null){
                              onDenied();
                            }
                            Navigator.pop(builder);
                          },
                          child: Text('no'.tr(), style: const TextStyle(color: Colors.grey)),
                        ),
                      ),
                    )
                  ],
                )
              ],
            ),
          );
        });
  }
}