import 'package:hive/hive.dart';

class SettingsPrefs {
  /// This function must return "light", "dark" or "system"
  Future<String> getThemeMode () async {
    LazyBox<String> settings = Hive.isBoxOpen("settings") ? Hive.lazyBox("settings") : await Hive.openLazyBox("settings");
    String? themeMode = await settings.get("themeMode", defaultValue: "system");
    return (themeMode ?? "system");
  }

  /// This function need values by "light", "dark" or "system"
  Future<void> setThemeMode (String mode) async {
    LazyBox<String> settings = Hive.isBoxOpen("settings") ? Hive.lazyBox("settings") : await Hive.openLazyBox("settings");
    switch (mode){
      case "system": {
        await settings.put("themeMode", mode);
      } break;
      case "dark": {
        await settings.put("themeMode", mode);
      } break;
      case "light":{
        await settings.put("themeMode", mode);
      } break;
      default: {
        throw Exception("String mode value must initialed with \"system\", \"dark\" or \"\"light");
      }
    }
  }
}