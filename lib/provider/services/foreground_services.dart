import 'dart:convert';
import 'dart:io';
import 'dart:isolate';
import 'dart:math';
import 'dart:typed_data';

import 'package:exif/exif.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:flutter_foreground_task/flutter_foreground_task.dart';
import 'package:photo_gallery/photo_gallery.dart';
import 'package:smartgallery/provider/database/database.dart';
import 'package:image/image.dart' as img ;
import 'package:smartgallery/utils/artificial_intelligence/photo_tagging.dart';
import 'dart:developer' as dev;

import '../../screens/navigation_screen.dart';
import '../../utils/artificial_intelligence/classifier.dart';
import '../../utils/artificial_intelligence/classifier_quant.dart';
import '../../utils/gallery/gallery.dart';
import '../../utils/map/coordinates_converter.dart';

void _startCallback() {
  // The setTaskHandler function must be called to handle the task in the background.
  FlutterForegroundTask.setTaskHandler(AddImagesTaskHandler());
}

class ForegroundTask {

  ReceivePort? _receivePort;


  Future<void> initial () async {
    await _initForegroundTask();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      // You can get the previous ReceivePort without restarting the service.
      if (await FlutterForegroundTask.isRunningService) {
        final newReceivePort = await FlutterForegroundTask.receivePort;
        _registerReceivePort(newReceivePort);
      }
    });
  }


  Future<void> startForegroundService() async {
    await _startForegroundTask();
  }

  Future<void> _initForegroundTask() async {
    await FlutterForegroundTask.init(
      androidNotificationOptions: AndroidNotificationOptions(
        channelId: 'notification_channel_id',
        channelName: 'Foreground Notification',
        channelDescription: 'This notification appears when the foreground service is running.',
        channelImportance: NotificationChannelImportance.LOW,
        priority: NotificationPriority.LOW,
        iconData: const NotificationIconData(
          resType: ResourceType.mipmap,
          resPrefix: ResourcePrefix.ic,
          name: 'launcher',
        ),
        buttons: [
          const NotificationButton(id: 'stop', text: 'CANCEL'),
        ],
      ),
      iosNotificationOptions: const IOSNotificationOptions(
          showNotification: true,
          playSound: true
      ),
      foregroundTaskOptions: const ForegroundTaskOptions(
          interval: 500,
          autoRunOnBoot: true,
          allowWifiLock: true
      ),
      printDevLog: true,
    );
  }


  Future<bool> _startForegroundTask() async {
    // "android.permission.SYSTEM_ALERT_WINDOW" permission must be granted for
    // onNotificationPressed function to be called.
    //
    // When the notification is pressed while permission is denied,
    // the onNotificationPressed function is not called and the app opens.
    //
    // If you do not use the onNotificationPressed or launchApp function,
    // you do not need to write this code.
    if (!await FlutterForegroundTask.canDrawOverlays) {
      final isGranted =
      await FlutterForegroundTask.openSystemAlertWindowSettings();
      if (!isGranted) {
        if (kDebugMode) {
          print('SYSTEM_ALERT_WINDOW permission denied!');
        }
        return false;
      }
    }

    // You can save data using the saveData function.
    await FlutterForegroundTask.saveData(key: 'customData', value: 'hello');

    bool reqResult;
    if (await FlutterForegroundTask.isRunningService) {
      reqResult = await FlutterForegroundTask.restartService();
    } else {
      reqResult = await FlutterForegroundTask.startService(
        notificationTitle: 'Foreground Service is running',
        notificationText: 'Tap to return to the app',
        callback: _startCallback,
      );
    }

    ReceivePort? receivePort;
    if (reqResult) {
      receivePort = await FlutterForegroundTask.receivePort;
    }

    return _registerReceivePort(receivePort);
  }


  bool _registerReceivePort(ReceivePort? receivePort) {
    _closeReceivePort();

    if (receivePort != null) {
      _receivePort = receivePort;
      _receivePort?.listen((message) async {
        if (message is int){
          var id = message;
          var row = await Databases.runQuery("SELECT * FROM Images WHERE id = $id");
          if (row != null) {
            var file = File(row[0]['path'].toString());
            var labels = await NavigationState.runModelOnImage(file);
            String generatedLabels = json.encode(labels).replaceAll("'", "");
            dev.log(generatedLabels);
            await Databases.modify("UPDATE Images SET labels = '$generatedLabels', is_tagged = '1' WHERE id = $id;");
          }
        }
      });
      return true;
    }

    return false;
  }

  void _closeReceivePort() {
    _receivePort?.close();
    _receivePort = null;
  }


  /// only call [addImages] method when all albums on devices added
  /// to app database
  /// only with calling
  /// ```dart
  /// addImages();
  /// ```
  /// you can pass
  static Future<void> addImages (SendPort port) async {
    // Classifier classifier = ClassifierQuant();
    // DartPluginRegistrant.ensureInitialized();
    var albums = await Gallery.getAlbums(mainThread: false);
    if (albums == null) return ;
    for (int i = 0 ; i < albums.length ; i++){
      var item = albums[i];
      var albumId = item['album_id'];
      Album album = Album.fromJson(
          {
            'id': albumId,
            'mediumType': item['medium_type'],
            'name': item['name'],
            'count': item['count']
          }
      );
      var count = await Databases.getCountFromTable("Images", suffix: "WHERE album_id = '$albumId'");
      final MediaPage page = await album.listMedia(
          skip: count,
          take: album.count - count,
          newest: true
      );
      for (int j = 0 ; j < page.items.length ; j++) {
        var image = page.items[j];
        File file = await image.getFile();
        List<int>? thumbnail;
        try {
          thumbnail = await image.getThumbnail(highQuality: true);
        } catch (e) {
          if(kDebugMode){
            print(e);
          }
        }
        final imageData = await readExifFromBytes(await file.readAsBytes());
        Map<String, double> latlng = {};
        if (imageData['GPS GPSLatitude'] != null && imageData['GPS GPSLongitude'] != null){
          try {
            latlng = CoordinatesConverter.convertDMSToDDStatic(imageData['GPS GPSLatitude'].toString(), imageData['GPS GPSLongitude'].toString());
          } catch(e){
            if(kDebugMode){
              print(e);
            }
          }
        }
        List<String> labels = [];

        String query = "INSERT INTO Images (image_id, name, path, thumbnail, album_id, is_tagged, labels, like, location, newest) VALUES ('${image.id}', '${image.title}', '${file.path}', 'thumbnailValue', '$albumId', '${labels.isEmpty ? '0' : '1'}', '${json.encode(labels).replaceAll("'", "")}', 'false', '${json.encode(latlng)}', 'false');";
        int? id = 0 ;
        try {
          id = await Databases.modify(
              query.replaceAll("thumbnailValue", json.encode(thumbnail))
          );
        } catch (e) {
          if (kDebugMode){
            dev.log(query);
            dev.log(e.toString());
          }
        }

        await Future.delayed(const Duration(milliseconds: 500));
        if (id != null){
          if (id != 0){
            port.send(id);
          }
        }
        await Future.delayed(const Duration(milliseconds: 500));

        count += 1 ;
        var percent = (count * 100) / page.total ;
        FlutterForegroundTask.updateService(
          notificationTitle: 'Adding "${item['name']}" images',
          notificationText: '${percent.toStringAsFixed(1)}% from ${item['name']} album',
          callback: null,
        );
      }
    }
    // var count = await Databases.getCountFromTable("Images");
    // var taggedImagesCount = await Databases.getCountFromTable("Images", suffix: "WHERE is_tagged = '1'");
    // var items = await Databases.runQuery("SELECT id,path FROM Images WHERE is_tagged = '0' ORDER BY id DESC");
    // if (items == null) return ;
    // for (int x = 0 ; x < items.length ; x++){
    //   File file = File(items[x]['path'].toString());
    //   int id = int.parse((items[x]['id']).toString());
    //   taggedImagesCount += 1 ;
    //   FlutterForegroundTask.updateService(
    //     notificationTitle: 'Tagged ${(taggedImagesCount)} of $count images',
    //     notificationText: 'At the time of tagging, you can use app',
    //     callback: null,
    //   );
    // }
  }


}


class AddImagesTaskHandler extends TaskHandler {
  SendPort? _sendPort;
  int _eventCount = 0;

  bool isBusy = false ;

  @override
  Future<void> onStart(DateTime timestamp, SendPort? sendPort) async {
    _sendPort = sendPort;
  }

  @override
  Future<void> onEvent(DateTime timestamp, SendPort? sendPort) async {
    if (!isBusy){
      ForegroundTask.addImages(sendPort!);
      isBusy = true ;
    }

    // Send data to the main isolate.

    _eventCount++;
  }

  @override
  Future<void> onDestroy(DateTime timestamp, SendPort? sendPort) async {
    // You can use the clearAllData function to clear all the stored data.
    await FlutterForegroundTask.clearAllData();
  }

  @override
  Future<void> onButtonPressed(String id) async {
    if (id == 'stop'){
      await FlutterForegroundTask.stopService();
    }
    // Called when the notification button on the Android platform is pressed.
    if (kDebugMode) {
      print('onButtonPressed >> $id');
    }
  }

  @override
  void onNotificationPressed() {
    _sendPort?.send('onNotificationPressed');
  }
}