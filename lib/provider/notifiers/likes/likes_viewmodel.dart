import 'dart:convert';

import 'package:flutter/foundation.dart';

import '../../../utils/gallery/gallery.dart';

class LikesViewModel with ChangeNotifier, DiagnosticableTreeMixin {
  bool loading = true ;
  List<Map<String, Object?>> images = [];
  Map<String, Object> changes = {};
  Map<String, Object> likes = {};

  Future<void> getImagesInAlbum({bool initial = true}) async {
    await Future.delayed(const Duration(milliseconds: 250));
    if (kDebugMode) {
      print("LOADING IMAGES ...");
    }
    if (initial){
      images.clear();
      loading = true ;
      notifyListeners();
    }
    await Future.delayed(const Duration(milliseconds: 250));
    var loadedImages = await Gallery.getFavoriteImages(9, images.length);
    if (loadedImages != null) {
      for (int i = 0 ; i < loadedImages.length ; i ++){
        images.add(loadedImages[i]);
      }
    }
    loading = false ;
    notifyListeners() ;
  }


  Future<void> changeItem (List<String> labels, int index, {bool? like}) async {
    if (like != null){
      images.removeAt(index);
      notifyListeners();
      return ;
    }
    var tags = json.encode(labels);
    if (tags != images[index]['labels']){
      changes[index.toString()] = tags ;
    } else {
      changes.remove(index.toString());
    }
  }

}