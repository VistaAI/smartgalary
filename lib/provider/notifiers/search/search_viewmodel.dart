import 'package:achievement_view/achievement_view.dart';
import 'package:achievement_view/achievement_widget.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:smartgallery/provider/database/database.dart';

class SearchViewModel with ChangeNotifier, DiagnosticableTreeMixin {

  final ScrollController _controller = ScrollController();
  ScrollController? get controller => _controller;

  BuildContext context ;
  SearchViewModel(this.context);

  String? album ;

  List<Map<String, Object?>> labels = [];
  Map<String, Object> changes = {};
  Map<String, Object> likes = {};
  bool loading = false ;


  bool progress = false ;

  Future<void> initialSearchScreen(String? albumId) async {
    album = albumId ;
    labels.clear();
    Future.delayed(const Duration(milliseconds: 250), (){
      notifyListeners();
    });
  }



  Future<void> search(BuildContext context, String word, {bool first = true}) async {
    if (progress){
      AchievementView(
          context,
          title: "Search in progress",
          subTitle: "Please wait a moment's ...",
          icon: const Icon(Icons.search_off, color: Colors.white),
          typeAnimationContent: AnimationTypeAchievement.fadeSlideToUp,
          borderRadius: BorderRadius.circular(360),
          color: Colors.deepOrange,
          textStyleTitle: const TextStyle(),
          textStyleSubTitle: const TextStyle(),
          alignment: Alignment.topCenter,
          duration: const Duration(milliseconds: 1500),
          isCircle: true
      ).show();
      return ;
    }
    progress = true ;
    if (word.isEmpty){
      labels.toString();
      return ;
    }

    if (first){
      loading = true ;
      labels.clear();
      _controller.addListener(() async {
        if (_controller.position.pixels == _controller.position.maxScrollExtent) {
          search(context, word, first: false);
        }
      });
    }


    var searchQuery = word.toLowerCase();
    String query = "SELECT * FROM Images WHERE ";
    if (album != null){
      query += "album_id = '$album' AND ";
    }
    query += "labels LIKE '%$searchQuery%' LIMIT 18 OFFSET ${labels.length}";
    var images = await (await Databases.initialDatabases()).rawQuery(query);
    for (int i = 0 ; i < images.length ; i++){
      var item = images[i];
      labels.add(item);
    }
    if (labels.isEmpty){
      AchievementView(
          context,
          title: "Not found",
          subTitle: album != null ? "No photos related to \"$word\" in this album" : "No photos related to \"$word\"",
          icon: const Icon(Icons.close, color: Colors.white),
          typeAnimationContent: AnimationTypeAchievement.fadeSlideToUp,
          borderRadius: BorderRadius.circular(360),
          color: Colors.redAccent,
          textStyleTitle: const TextStyle(),
          textStyleSubTitle: const TextStyle(),
          alignment: Alignment.topCenter,
          duration: const Duration(milliseconds: 1500),
          isCircle: true
      ).show();
    }
    progress = false ;
    loading = false ;
    notifyListeners();
    // if (labels.isNotEmpty){
    //   Future.delayed(const Duration(milliseconds: 250), (){
    //     _controller.animateTo(_controller.position.maxScrollExtent, duration: const Duration(milliseconds: 500), curve: Curves.easeOut);
    //   });
    // }
  }







}