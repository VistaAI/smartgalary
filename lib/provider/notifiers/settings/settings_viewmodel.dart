import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:smartgallery/theme/widgets/tm_text.dart';
import 'package:dropbox_client/dropbox_client.dart';
import 'package:smartgallery/utils/notifications/alert.dart';

class SettingsViewModel with ChangeNotifier, DiagnosticableTreeMixin {


  Future<void> showBackupAccounts(BuildContext context) async {
    await Dropbox.init("reg11w404ra1uh0", "reg11w404ra1uh0", "2ifyyj5siyv3qtf");
    var dropBoxToken = await Dropbox.getAccessToken();
    String? accountName = "";
    if (dropBoxToken != null){
      accountName = await Dropbox.getAccountName();
    }
    Alert alert = Alert(context);
    showModalBottomSheet(
      context: context,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(10.0),
          topRight: Radius.circular(10.0)
        ),
      ),
      backgroundColor: Theme.of(context).backgroundColor,
      builder: (builder){
        var itemHeight = 60.0 ;
        var itemCount = dropBoxToken != null ? 1 : 2;
        return Container(
          height: ((itemCount) * itemHeight) + 60,
          padding: const EdgeInsets.only(top: 14),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Padding(
                padding: EdgeInsets.only(left: 16, right: 16, bottom: 16, top: 8),
                child: TMText("Accounts", fontWeight: FontWeight.bold, fontSize: 16, letterSpacing: 1.5),
              ),
              SizedBox(
                height: (itemCount) * itemHeight,
                child: ListView.builder(itemCount: (itemCount), itemBuilder: (BuildContext context, int index){
                  if (index == (itemCount - 1) && dropBoxToken == null){
                    return Container(
                      height: 40,
                      margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: Colors.green,
                          width: 1.2
                        ),
                        borderRadius: BorderRadius.circular(4)
                      ),
                      child: TextButton(
                        onPressed: () async {
                          if (dropBoxToken == null){
                            await Dropbox.authorize();
                            Navigator.pop(builder);
                          } else {
                            Dropbox.unlink();
                            alert.success("DropBox Logout", "Logout from DropBox Successfully");
                            Navigator.pop(builder);
                          }
                        },
                        child: Text(dropBoxToken == null ? "Add Account" : "Logout"),
                      ),
                    );
                  }
                  if (dropBoxToken == null){
                    return Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16.0),
                      child: SizedBox(
                        height: itemHeight,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisSize: MainAxisSize.min,
                          children: const [
                            TMText("Not find DropBox Account, Try add once")
                          ],
                        ),
                      ),
                    );
                  } else {
                    return Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16.0),
                      child: SizedBox(
                        height: itemHeight,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Stack(
                              children: [
                                SizedBox(
                                  width: 40,
                                  height: 40,
                                  child: Image.asset("assets/images/png/dropbox.png", fit: BoxFit.contain),
                                ),
                              ],
                            ),
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.only(right: 16, left: 16, bottom: 3),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    TMText(accountName.toString(), fontWeight: FontWeight.bold, fontSize: 14),
                                    const SizedBox(height: 2),
                                    const TMText("DropBox", fontWeight: FontWeight.w400, fontSize: 12)
                                  ],
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(right: 8, left: 8),
                              child: TextButton(
                                onPressed: (){
                                  logout(alert);
                                  Navigator.pop(builder);
                                },
                                child: const Text("Logout"),
                              ),
                            )
                          ],
                        ),
                      ),
                    );
                  }
                }),
              )
            ],
          ),
        );
      }
    );
  }

  bool dropBoxLogedIn = false ;
  String dropBoxAccountName = "";


  Future<void> logout(Alert alert) async {
    Dropbox.unlink();
    alert.success("DropBox Logout", "Logout from DropBox Successfully");
    dropBoxLogedIn = false ;
    notifyListeners();
  }

  Future<void> checkDropBoxIsLogedIn() async {
    String? accessToken = await Dropbox.getAccessToken();
    if (accessToken != null){
      String? name = await Dropbox.getAccountName();
      if (name != null){
        dropBoxAccountName = name ;
      }
      dropBoxLogedIn = true ;
      notifyListeners();
    } else {
      dropBoxLogedIn = false ;
      notifyListeners();
    }
  }

}