import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:photo_gallery/photo_gallery.dart';

import '../../../utils/gallery/gallery.dart';
import '../../database/database.dart';
import 'dart:developer' as dev;

class AlbumViewModel with ChangeNotifier, DiagnosticableTreeMixin {


  final ScrollController _controller = ScrollController();
  ScrollController? get controller => _controller;
  bool loading = true ;
  bool loadMore = true ;

  List<Map<String, Object?>> images = [];
  Map<String, Object> changes = {};
  Map<String, Object> likes = {};


  Future<void> deleteFile(BuildContext context, File file, int index) async {
    try {
      await Databases.modify("UPDATE Images SET album_id = 'DELETED' WHERE path = '${file.path}'");
      images.removeAt(index);
    } catch (e){
      if (kDebugMode){
        print(e);
      }
    }
    notifyListeners();
  }


  Future<void> getImagesInAlbum({String? albumId, bool initial = true, int? limit}) async {
    loading = true ;
    if (initial){
      images.clear();
      await Future.delayed(const Duration(milliseconds: 500));
    }
    notifyListeners();

    var loadedImages = await Gallery.getImagesInAlbum(9, images.length, albumId: albumId);
    if (loadedImages != null) {
      for (int i = 0 ; i < loadedImages.length ; i ++){
        if (!images.contains(loadedImages[i])){
          images.add(loadedImages[i]);
        }
      }
    }

    if (initial){
      _controller.addListener(() async {
        if (_controller.position.pixels == _controller.position.maxScrollExtent) {
          await getImagesInAlbum(albumId: albumId, initial: false);
        }
      });
      notifyListeners();
      var loadedImages2 = await getImagesInAlbum(albumId: albumId, initial: false);
    } else {
      loading = false ;
      notifyListeners();
    }

    // if (kDebugMode) {
    //   print("LOADING IMAGES ...");
    // }
    // if (initial){
    //   images.clear();
    // }
    // var loadedImages = await Gallery.getImagesInAlbum(9, images.length, albumId: albumId);
    // if (loadedImages != null) {
    //   for (int i = 0 ; i < loadedImages.length ; i ++){
    //     if (!images.contains(loadedImages[i])){
    //       images.add(loadedImages[i]);
    //     }
    //   }
    // }
    // if (initial){
    //   var loadedImages = await Gallery.getImagesInAlbum(9, images.length, albumId: albumId);
    //   if (loadedImages != null) {
    //     for (int i = 0 ; i < loadedImages.length ; i ++){
    //       if (!images.contains(loadedImages[i])){
    //         images.add(loadedImages[i]);
    //       }
    //     }
    //   }
    //   _controller.addListener(() async {
    //     if (_controller.position.pixels == _controller.position.maxScrollExtent) {
    //       int count = await Databases.getCountFromTable("Images", suffix: "WHERE album_id = '$albumId'");
    //       if (images.length < count){
    //         if ((count - images.length) < 9){
    //           await getImagesInAlbum(albumId: albumId, initial: false, limit: (count - images.length));
    //         } else {
    //           await getImagesInAlbum(albumId: albumId, initial: false);
    //         }
    //       }
    //     }
    //   });
    // }
    //
    // loading = false ;
    // notifyListeners();

    // if (initial){
    //   notifyListeners();
    //   await getImagesInAlbum(albumId: albumId, initial: false);
    //   await getImagesInAlbum(albumId: albumId, initial: false);
    //   await getImagesInAlbum(albumId: albumId, initial: false);
    //   _controller.addListener(() async {
    //     if (_controller.position.pixels == _controller.position.maxScrollExtent) {
    //       int count = await Databases.getCountFromTable("Images", suffix: "WHERE album_id = '$albumId'");
    //       if (images.length < count){
    //         if ((count - images.length) < 9){
    //           await getImagesInAlbum(albumId: albumId, initial: false, limit: (count - images.length));
    //         } else {
    //           await getImagesInAlbum(albumId: albumId, initial: false);
    //         }
    //       }
    //     }
    //   });
    // } else {
    //
    // }
  }


  Future<void> changeItem (List<String> labels, int index, {bool? like}) async {
    if (like != null){
      if (like.toString() != images[index]['like']){
        likes[index.toString()] = like.toString();
      } else {
        likes.remove(index.toString());
      }
      return ;
    }
    var tags = json.encode(labels);
    if (tags != images[index]['labels']){
      changes[index.toString()] = tags ;
    } else {
      changes.remove(index.toString());
    }
  }
}