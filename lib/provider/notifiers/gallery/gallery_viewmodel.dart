import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:smartgallery/screens/gallery/album_screen.dart';
import 'package:smartgallery/utils/gallery/gallery.dart';

class GalleryViewModel with ChangeNotifier, DiagnosticableTreeMixin {

  // Loading set to false when complete initialing
  bool loading = true ;
  bool isEmpty = false ;

  // show albums from this variable in gallery_screen.dart
  List<Map<String, Object?>>? albums ;

  /// Add albums in device to app in
  /// ```dart
  /// initialGalleryScreen();
  /// ```
  /// function
  /// no requires input
  /// ```dart
  /// context.read<GalleryViewModel>().initialApp();
  /// ```
  Future<void> initialGalleryScreen () async {
    loading = true ;
    await Future.delayed(const Duration(milliseconds: 250), (){
      notifyListeners();
    });
    List<Map<String, Object?>>? inAppAlbums = await Gallery.getAlbums();
    albums = inAppAlbums ;
    if ( inAppAlbums?.isEmpty == true ){
      isEmpty = true ;
    } else {
      isEmpty = false ;
    }
    loading = false ;
    notifyListeners() ;
  }


  /// This function show images an album with id passed on [albumId]
  Future<void> openAlbum(BuildContext context, {String? name, String? albumId}) async {
    Navigator.of(context).push(
        MaterialPageRoute(builder: (builder){
          return AlbumScreen(albumId: albumId, title: name);
        })
    );
  }
}