import 'dart:convert';

import 'package:dropbox_client/dropbox_client.dart';
import 'package:flutter/foundation.dart';

import 'dart:io';
import 'dart:typed_data';
import 'dart:developer' as dev;

import 'package:achievement_view/achievement_view.dart';
import 'package:achievement_view/achievement_widget.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image/image.dart' as img;
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_editor_plus/image_editor_plus.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:persian_number_utility/persian_number_utility.dart';
import 'package:r_scan/r_scan.dart';
import 'package:share_plus/share_plus.dart';
import 'package:string_validator/string_validator.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:wifi_iot/wifi_iot.dart';

import '../../../utils/artificial_intelligence/classifier.dart';
import '../../../utils/artificial_intelligence/classifier_quant.dart';

class ImageViewModel with ChangeNotifier, Diagnosticable {

  RScanResult? _QRCodeResult ;
  String? qrTitle ;
  List? _recognitionsList ;
  final bool _isWorking = false ;
  double? _imgHeight ;
  double? _imgWidth ;

  RScanResult? get QRCodeResult => _QRCodeResult;
  List? get recognitionsList => _recognitionsList;
  bool get isWorking => _isWorking;
  double? get imgHeight => _imgHeight;
  double? get imgWidth => _imgWidth;

  List<String> tags = [];
  List<String> options = [];


  void shareImage(File file, String? title) {
    Share.shareFiles([(file.path)], text: '$title\nImages available on Sensifai' );
  }


  Future<Map<String, double>> predict(File image) async {
    Map<String, double> response = {};
    // Classifier classifier = ClassifierQuant();
    // img.Image? imageInput = img.decodeImage(await image.readAsBytes());
    // if (imageInput == null) return {};
    // var response = classifier.predict(imageInput);
    return response;
  }

  Future<void> readQR(File file) async {
    tags = [];
    options = [];
    _QRCodeResult = null ;
    qrTitle = null ;
    RScanResult result = await RScan.scanImagePath(file.path);
    if (result.message != null){
      _QRCodeResult = result ;
      if(isURL(result.message.toString())) {
        qrTitle = 'open_link'.tr();
      } else if (_QRCodeResult!.message.toString().contains(":")){
        if (_QRCodeResult!.message.toString().split(":")[0] == "WIFI"){
          qrTitle = 'connect_wifi'.tr();
        } else if (_QRCodeResult!.message.toString().split(":")[0] == "geo"){
          qrTitle = 'open_in_map'.tr();
        } else {
          qrTitle = 'open_whatsapp'.tr();
        }
      } else if(isPhoneNumber(_QRCodeResult?.message)) {
        qrTitle = 'make_a_call'.tr();
      } else {
        qrTitle = _QRCodeResult?.message;
      }
    } else {
      qrTitle = null;
    }
    notifyListeners();
  }

  bool isPhoneNumber(String? text){
    if (text == null) return false;
    return text.isValidIranianMobileNumber() ;
  }

  Future<void> editImage(File file, BuildContext context) async {
    // OpenFile.open(file.path, type: "image/*");
    // Navigator.push(context, MaterialPageRoute(builder: (builder) => EditImage(file)));
    var image = await file.readAsBytes();
    await Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => Scaffold(
            body: ImageEditor(
                image: image,
                appBar: Colors.green,
                allowCamera: false,
                allowGallery: false,
                allowMultiple: false
            ),
          ),
        )
    ).then((value) async {
      if (value == null) return ;
      var images = value as Uint8List;
      var path = (await getExternalStorageDirectory())?.path;
      if (path == null) return ;
      path += "/Sensifai/Edits/";
      var directory = Directory(path);
      if (!(await directory.exists())){
        await directory.create(recursive: true);
      }
      File f = File(path + "${DateTime.now().millisecondsSinceEpoch}.png");
      // if (await file.exists()){
      //   if (kDebugMode) {
      //     print("File exist: " + file.path);
      //   }
      //   AchievementView(
      //       context,
      //       title: "Already exist",
      //       subTitle: "This file exist on other directory",
      //       icon: Icon(Icons.close, color: Colors.white),
      //       typeAnimationContent: AnimationTypeAchievement.fadeSlideToUp,
      //       borderRadius: BorderRadius.circular(8),
      //       color: Colors.redAccent,
      //       textStyleTitle: TextStyle(),
      //       textStyleSubTitle: TextStyle(),
      //       alignment: Alignment.topCenter,
      //       duration: Duration(milliseconds: 1500),
      //       isCircle: true
      //   ).show();
      //   return;
      // }
      await f.writeAsBytes(images);
      await f.create(recursive: true);

      if (kDebugMode){
        print(f.path);
      }
      AchievementView(
          context,
          title: "Image saved on",
          subTitle: f.path,
          //onTab: _onTabAchievement,
          icon: const Icon(Icons.done, color: Colors.white),
          typeAnimationContent: AnimationTypeAchievement.fadeSlideToUp,
          borderRadius: BorderRadius.circular(8),
          color: Colors.green,
          textStyleTitle: const TextStyle(),
          textStyleSubTitle: const TextStyle(),
          alignment: Alignment.topCenter,
          duration: const Duration(milliseconds: 1500),
          isCircle: true,
          onTap: () async {
            await OpenFile.open(f.path);
          }
      ).show();
    });

  }


  Future<void> launchURL(String? qrTitle, String _url) async {
    try {
      if (qrTitle == 'open_link'.tr()){
        await launchUrl(Uri.parse(_url), mode: LaunchMode.externalApplication);
      } else if (qrTitle == 'make_a_call'.tr()){
        await launchUrl(Uri.parse("tel:$_url"));
      } else if (qrTitle == 'open_whatsapp'.tr()){
        await launchUrl(Uri.parse(_url));
      } else if (_url.contains(":")){
        List<String> splitDetails = _url.split(":");
        if (splitDetails[0] == "WIFI"){
          bool wifiIsEnabled = await WiFiForIoTPlugin.isEnabled();
          if (wifiIsEnabled){
            String ssid = splitDetails[2].split(";")[0];
            String type = splitDetails[3].split(";")[0];
            String password = splitDetails[4].replaceAll(";", "");
            await WiFiForIoTPlugin.setEnabled(true);
            await WiFiForIoTPlugin.connect(
                ssid,
                password: password,
                security: type == "WPA" ? NetworkSecurity.WPA : NetworkSecurity.WEP
            );
          } else {
            Fluttertoast.showToast(msg: 'please_enable_Wi-Fi'.tr());
          }

        } else if (splitDetails[0] == "geo"){
          await launchUrl(Uri.parse(_url));
        }
      } else {
        Clipboard.setData(ClipboardData(text: _url));
        Fluttertoast.showToast(msg: 'text_copied_to_clipboard'.tr());
      }

    } catch (e) {
      if (kDebugMode){
        print(e);
      }
    }
  }



  void setOptionsAndTags (List<String> val) async {
    await Future.delayed(const Duration(milliseconds: 250));
    tags = val ;
    options = val ;
    notifyListeners();
  }

  void setOptions(List<String> val){
    options = val ;
    notifyListeners();
  }

  void setTags(List<String> val){
    tags = val ;
    notifyListeners();
  }

  void valueChanged(List<String> val) {
    tags = val ;
    // options = val ;
    notifyListeners();
  }

  bool imageDownloaded = false ;
  bool functionRunned = false ;

  void changeUploadedStatus (bool status){
    imageDownloaded = status ;
    notifyListeners();
  }

  Future<void> download(File file) async {
    imageDownloaded = false ;
    functionRunned = false ;
    await Future.delayed(const Duration(milliseconds: 200));
    notifyListeners();
    final filepath = file.path;
    var split = file.path.split("/");
    var dot = split[split.length - 1].split(".");
    var uploaded = "${file.parent.path}/${dot[dot.length - 2]}IF${dot[dot.length - 1]}IF.sgei";
    var uploadedSplit = uploaded.split("/");
    var directory = await getExternalStorageDirectory();
    if (directory == null){
      return;
    }
    var downloadPath = "${directory.path}/${uploadedSplit[uploadedSplit.length - 1].replaceAll("sgei", dot[dot.length - 1]).replaceAll("IF${dot[dot.length - 1]}IF", "")}";
    try {
      final result = await Dropbox.download(uploaded, downloadPath, (downloaded, total) {
        if (total == downloaded){
          imageDownloaded = true ;
          functionRunned = true ;
          notifyListeners();
        }
        print("$downloaded / $total");
      });
      print(result);
    } catch (e){
      imageDownloaded = false ;
      functionRunned = true ;
      notifyListeners();
    }
    functionRunned = true ;
    notifyListeners();
  }
}
