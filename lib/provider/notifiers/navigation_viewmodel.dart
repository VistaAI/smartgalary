import 'dart:convert';
import 'dart:io';
import 'dart:isolate';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:photo_gallery/photo_gallery.dart';
import 'package:sizer/sizer.dart';
import 'package:smartgallery/provider/services/foreground_services.dart';
import 'package:smartgallery/theme/widgets/tm_icon.dart';
import 'package:smartgallery/theme/widgets/tm_text.dart';
import 'package:smartgallery/utils/gallery/gallery.dart';
import 'package:password_strength/password_strength.dart';
import 'package:smartgallery/utils/notifications/alert.dart';
import 'package:http/http.dart' as http;

class NavigationViewModel with ChangeNotifier, DiagnosticableTreeMixin {


  ReceivePort? _receivePort;
  ReceivePort? get receivePort => _receivePort;

  int _currentIndex = 0;
  int get index => _currentIndex;

  final PageController _controller = PageController(initialPage: 0);
  PageController get controller => _controller;

  bool loading = true ;


  void changePage(int page) {
    _currentIndex = page;
    _controller.jumpToPage(page);
    notifyListeners();
  }

  /// Add albums in device to app in [initialApp] function
  /// no requires input
  /// ```dart
  /// context.read<GalleryViewModel>().initialApp();
  /// ```
  Future<void> initialApp () async {
    loading = true ;
    await Future.delayed(const Duration(milliseconds: 250), (){
      notifyListeners();
    });
    List<Map<String, Object?>>? inAppAlbums = await Gallery.getAlbums();
    if (inAppAlbums == null) {
      loading = false ;
      notifyListeners();
      return ;
    }
    if (inAppAlbums.isEmpty){
      if (kDebugMode) {
        print("ADDING ALBUMS ... ");
      }
      await _initialAlbumsOnDevice();
      return ;
    }
    loading = false ;
    ForegroundTask foregroundTask = ForegroundTask();
    await foregroundTask.initial();
    await foregroundTask.startForegroundService();
    notifyListeners();
  }

  Future<void> _initialAlbumsOnDevice() async {
    List<Album> deviceAlbums = await Gallery.getAlbumsFromDevice();
    if (deviceAlbums.isEmpty){
      loading = false ;
      notifyListeners();
      return ;
    }
    await _addDeviceAlbumsToDatabase(deviceAlbums);
  }

  Future<void> _addDeviceAlbumsToDatabase(List<Album> deviceAlbums) async {
    for (int i = 0 ; i < deviceAlbums.length ; i++){
      var item = deviceAlbums[i];
      if (kDebugMode) {
        print("ADDING ${item.name} ALBUM ... ");
      }
      List<int> thumbnail = await item.getThumbnail(highQuality: true);
      await Gallery.addNewAlbumToApp("'${item.name}', '${item.isAllAlbum}', '${item.id}', '${json.encode(thumbnail)}', '${mediumTypeToJson(item.mediumType)}', '${item.count}'");

      if (i == (deviceAlbums.length - 1)){
        if (kDebugMode) {
          print("ALL ALBUMS ADDED COMPLETED :)");
        }
      }
    }

    ForegroundTask foregroundTask = ForegroundTask();
    await foregroundTask.initial();
    await foregroundTask.startForegroundService();
    loading = false ;
    notifyListeners() ;
  }

  Future <void> showLoginModal(BuildContext context) async {
    showModalBottomSheet(
        context: context,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(16),
            topRight: Radius.circular(16)
          ),
        ),
        isScrollControlled: true,
        backgroundColor: Theme.of(context).backgroundColor,
        builder: (builder){
          return Padding(
            padding: EdgeInsets.only(top: 16, left: 16, right: 16, bottom: MediaQuery.of(context).viewInsets.bottom + 16),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8),
                  child: Center(
                    child: Image.asset("assets/images/png/logo_dark.png", width: 150, height: 41),
                  ),
                ),
                const SizedBox(
                  height: 24,
                ),
                const Center(
                    child: TMText("Account login", fontWeight: FontWeight.bold, fontSize: 18)
                ),
                const SizedBox(
                  height: 24,
                ),
                SizedBox(
                  height: 50,
                  child: TextFormField(
                    keyboardType: TextInputType.emailAddress,
                    textInputAction: TextInputAction.next,
                    decoration: InputDecoration(
                      labelText: "Email",
                      border: OutlineInputBorder(
                        borderSide: const BorderSide(
                          color: Colors.green,
                          width: 0.1,
                          style: BorderStyle.solid
                        ),
                        borderRadius: BorderRadius.circular(8)
                      )
                    ),
                  ),
                ),
                const SizedBox(height: 8),
                SizedBox(
                  height: 50,
                  child: TextFormField(
                    keyboardType: TextInputType.visiblePassword,
                    obscureText: true,
                    textInputAction: TextInputAction.done,
                    decoration: InputDecoration(
                        labelText: "Password",
                        border: OutlineInputBorder(
                            borderSide: const BorderSide(
                                color: Colors.green,
                                width: 0.1,
                                style: BorderStyle.solid
                            ),
                            borderRadius: BorderRadius.circular(8)
                        )
                    ),
                  ),
                ),
                const SizedBox(height: 16),
                SizedBox(
                  height: 45,
                  width: double.infinity,
                  child: ElevatedButton(
                      onPressed: (){},
                      child: const Text("Login", style: TextStyle(fontWeight: FontWeight.bold))
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 24),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      InkWell(
                        onTap: (){
                          Navigator.pop(builder);
                          Future.delayed(const Duration(milliseconds: 200), (){
                            showRegisterModal(context);
                          });
                        },
                        child: const TMText("Create account", color: Colors.blueAccent)
                      ),
                      const Expanded(
                        child: SizedBox(
                          height: 0,
                          width: 0,
                        ),
                      ),
                      const TMText("Recover password", color: Colors.blueAccent),
                    ],
                  ),
                )
              ],
            ),
          );
        }
    );
  }

  double strength = 0.0 ;
  File? avatar;
  var first = "";
  var last = "";
  var mail = "";
  var pass = "";
  var correct = false ;
  Future <void> showRegisterModal(BuildContext context) async {
    Alert alert = Alert(context);
    showModalBottomSheet(
        context: context,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(16),
              topRight: Radius.circular(16)
          ),
        ),
        isScrollControlled: true,
        enableDrag: true,
        backgroundColor: Theme.of(context).backgroundColor,
        builder: (builder){
          return StatefulBuilder(builder: (BuildContext stfulContext, StateSetter setState){
            return SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.only(top: 16, left: 16, right: 16, bottom: MediaQuery.of(context).viewInsets.bottom + 8),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8),
                      child: Center(
                        child: Image.asset("assets/images/png/logo_dark.png", width: 150, height: 41),
                      ),
                    ),
                    const SizedBox(
                      height: 24,
                    ),
                    const Center(
                        child: TMText("Register an account", fontWeight: FontWeight.bold, fontSize: 18)
                    ),
                    const SizedBox(
                      height: 24,
                    ),
                    Center(
                      child: SizedBox(
                        width: 75,
                        height: 75,
                        child: InkWell(
                          onTap: () async {
                            final ImagePicker _picker = ImagePicker();
                            XFile? _picked = await _picker.pickImage(source: ImageSource.gallery);
                            if (_picked != null){
                              setState((){
                                avatar = File(_picked.path);
                              });
                            }
                          },
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(360),
                            child: avatar != null ? Image.file(avatar!, fit: BoxFit.cover) : const TMIcon(Icons.account_circle, size: 75),
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 24,
                    ),
                    SizedBox(
                      height: 50,
                      width: 100.w,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SizedBox(
                            height: 50,
                            width: 45.w,
                            child: TextFormField(
                              keyboardType: TextInputType.name,
                              textCapitalization: TextCapitalization.words,
                              textInputAction: TextInputAction.next,
                              initialValue: first,
                              onChanged: (value){
                                first = value;
                              },
                              decoration: InputDecoration(
                                  labelText: "First name",
                                  border: OutlineInputBorder(
                                      borderSide: const BorderSide(
                                          color: Colors.green,
                                          width: 0.1,
                                          style: BorderStyle.solid
                                      ),
                                      borderRadius: BorderRadius.circular(8)
                                  )
                              ),
                            ),
                          ),
                          const SizedBox(width: 4),
                          SizedBox(
                            height: 50,
                            width: 45.w,
                            child: TextFormField(
                              keyboardType: TextInputType.name,
                              textCapitalization: TextCapitalization.words,
                              textInputAction: TextInputAction.next,
                              initialValue: last,
                              onChanged: (value){
                                last = value;
                              },
                              decoration: InputDecoration(
                                  labelText: "Last name",
                                  border: OutlineInputBorder(
                                      borderSide: const BorderSide(
                                          color: Colors.green,
                                          width: 0.1,
                                          style: BorderStyle.solid
                                      ),
                                      borderRadius: BorderRadius.circular(8)
                                  )
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(height: 8),
                    SizedBox(
                      height: 50,
                      width: 92.w,
                      child: TextFormField(
                        keyboardType: TextInputType.emailAddress,
                        textInputAction: TextInputAction.next,
                        initialValue: mail,
                        onChanged: (value){
                          mail = value;
                        },
                        decoration: InputDecoration(
                            labelText: "Email",
                            border: OutlineInputBorder(
                                borderSide: const BorderSide(
                                    color: Colors.green,
                                    width: 0.1,
                                    style: BorderStyle.solid
                                ),
                                borderRadius: BorderRadius.circular(8)
                            )
                        ),
                      ),
                    ),
                    const SizedBox(height: 8),
                    SizedBox(
                      height: 50,
                      width: 100.w,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SizedBox(
                            height: 50,
                            width: 45.w,
                            child: TextFormField(
                              keyboardType: TextInputType.visiblePassword,
                              obscureText: true,
                              textInputAction: TextInputAction.next,
                              initialValue: pass,
                              onChanged: (String password){
                                pass = password;
                                if (password.isNotEmpty){
                                  setState((){
                                    strength = estimatePasswordStrength(password);
                                  });
                                } else {
                                  setState((){
                                    strength = 0.0;
                                  });
                                }
                              },
                              decoration: InputDecoration(
                                  labelText: "Password",
                                  border: OutlineInputBorder(
                                      borderSide: const BorderSide(
                                          color: Colors.green,
                                          width: 0.1,
                                          style: BorderStyle.solid
                                      ),
                                      borderRadius: BorderRadius.circular(8)
                                  )
                              ),
                            ),
                          ),
                          const SizedBox(width: 4),
                          SizedBox(
                            height: 50,
                            width: 45.w,
                            child: TextFormField(
                              initialValue: correct ? pass : null,
                              keyboardType: TextInputType.visiblePassword,
                              obscureText: true,
                              textInputAction: TextInputAction.done,
                              onChanged: (value){
                                if (pass == value){
                                  if (!correct){
                                    setState((){
                                      correct = true ;
                                    });
                                  }
                                } else {
                                  if (correct){
                                    setState((){
                                      correct = false ;
                                    });
                                  }
                                }
                              },
                              decoration: InputDecoration(
                                  labelText: "Repeat",
                                  suffix: correct ? const Icon(Icons.done, size: 15, color: Colors.green) : const Icon(Icons.lock_clock, size: 15, color: Colors.red),
                                  border: OutlineInputBorder(
                                      borderSide: const BorderSide(
                                          color: Colors.green,
                                          width: 0.1,
                                          style: BorderStyle.solid
                                      ),
                                      borderRadius: BorderRadius.circular(8)
                                  )
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(height: 16),
                    Row(
                      children: [
                        const Padding(
                          padding: EdgeInsets.symmetric(horizontal: 4),
                          child: TMText("Password strong"),
                        ),
                        const Expanded(child: SizedBox(width: 4, height: 0)),
                        TMText("${(strength * 100).toStringAsFixed(0)}%"),
                        const Expanded(child: SizedBox(width: 4, height: 0)),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 4),
                          child: SizedBox(
                            width: 45.w,
                            height: 4,
                            child: LinearProgressIndicator(
                              backgroundColor: Colors.grey,
                              color: strength < 0.6 ? Colors.redAccent : (strength < 0.94 ? Colors.orangeAccent : Colors.green),
                              value: strength
                            ),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 16),
                    SizedBox(
                      height: 45,
                      width: double.infinity,
                      child: ElevatedButton(
                          onPressed: () async {
                            if (first.isEmpty){
                              alert.error("First name required", "Please fill first name field");
                              return;
                            }
                            if (last.isEmpty){
                              alert.error("Last name required", "Please fill last name field");
                              return;
                            }
                            if (mail.isEmpty){
                              alert.error("Email required", "Please fill email field");
                              return;
                            }

                            bool validMail = RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(mail);
                            if (!validMail){
                              alert.error("Email is not valid", "Please use valid mail address");
                              return;
                            }

                            if (strength < 0.94){
                              alert.error("Password not strong", "Please use strong password");
                              return;
                            }

                            if (!correct){
                              alert.error("Passwords not match", "Please check passwords use");
                              return;
                            }


                            showDialog(context: context, builder: (builder){
                              registerRestApiRequest(
                                first,
                                last,
                                mail,
                                pass,
                                avatar
                              ).then((value) {
                                Navigator.pop(builder);
                              }). catchError((e){
                                if (kDebugMode) {
                                  print(e);
                                }
                                Navigator.pop(builder);
                              });
                              return AlertDialog(
                                title: const Text("Loading ..."),
                                content: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisSize: MainAxisSize.min,
                                  children: const [
                                    SizedBox(
                                      width: 35,
                                      height: 35,
                                      child: CircularProgressIndicator(strokeWidth: 1.5),
                                    ),
                                    SizedBox(width: 16),
                                    Text("Please wait ...")
                                  ],
                                ),
                              );
                            });

                          },
                          child: const Text("Confirm", style: TextStyle(fontWeight: FontWeight.bold))
                      ),
                    ),
                    const SizedBox(height: 24),
                  ],
                ),
              ),
            );
          });
        }
    );
  }


  Future<void> registerRestApiRequest(String first, String last, String mail, String pass, File? avatar) async {
    var request = http.MultipartRequest('POST', Uri.parse('http://192.168.1.92:3000/users/register'));
    request.fields.addAll({
      'first_name': first,
      'last_name': last,
      'mail': mail,
      'password': pass
    });
    if (avatar != null){
      request.files.add(await http.MultipartFile.fromPath('avatar', (avatar.path).toString()));
    }

    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      if (kDebugMode) {
        print(await response.stream.bytesToString());
      }
    } else {
      if (kDebugMode) {
        print(response.reasonPhrase);
      }
    }
  }

}