import 'dart:convert';

import 'package:dropbox_client/dropbox_client.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class FileManagerViewModel with ChangeNotifier, DiagnosticableTreeMixin {
  BuildContext context;
  FileManagerViewModel(this.context);
  List? result ;
  String path = '';

  Future<void> loadDirectory(BuildContext context, {String path = ''}) async {
    showDialog(context: context, builder: (builder){
      load(
        path: path
      ).then((value) {
        Navigator.pop(builder);
      }). catchError((e){
        if (kDebugMode) {
          print(e);
        }
        Navigator.pop(builder);
      });
      return AlertDialog(
        title: const Text("Loading ..."),
        content: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: const [
            SizedBox(
              width: 35,
              height: 35,
              child: CircularProgressIndicator(strokeWidth: 1.5),
            ),
            SizedBox(width: 16),
            Text("Please wait ...")
          ],
        ),
      );
    });
  }


  Future<void> load({String path = ''}) async {
    final result = await Dropbox.listFolder(path);
    this.result = result ;
    this.path = path ;
    if (kDebugMode) {
      print("${result.runtimeType} - ${json.encode(result)}");
    }
    notifyListeners();
    }
}