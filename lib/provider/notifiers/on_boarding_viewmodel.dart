import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_foreground_task/flutter_foreground_task.dart';
import 'package:permission_handler/permission_handler.dart' as access;
import 'package:smartgallery/utils/permissions/permission_manager.dart';
import '../../screens/navigation_screen.dart';

class OnBoardingViewModel with ChangeNotifier, DiagnosticableTreeMixin {


  void onIntroEnd(BuildContext context) {
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(builder: (builder) => const Navigation())
    );
  }

  void requestStoragePermission (BuildContext context) async {
    await PermissionManager.requestPermission(
      context,
      access.Permission.storage,
      title: 'permission_storage_title',
      description: 'permission_storage_description',
      icon: Icons.sd_storage,
      onDenied: (){

      },
      onGranted: () async {
        if (!await FlutterForegroundTask.canDrawOverlays) {
          final isGranted = await FlutterForegroundTask.openSystemAlertWindowSettings();
          if (isGranted) {
            onIntroEnd(context);
          }
        } else {
          onIntroEnd(context);
        }
      },
    );
  }
}
