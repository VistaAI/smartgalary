import 'package:path/path.dart';
import 'package:photo_gallery/photo_gallery.dart';
import 'package:sqflite/sqflite.dart';

class Databases {

  static Database? db ;


  /// Initialing & create tables if not created.
  static Future<Database> initialDatabases () async {
    if (db != null){
      return db!;
    }
    var databasesPath = await getDatabasesPath();
    String path = join(databasesPath, 'sensifai.db');
    db = await openDatabase(path, version: 1, onCreate: (Database db, int version) async {
      await db.execute('CREATE TABLE Albums (id INTEGER PRIMARY KEY, name TEXT, isAllAlbum BOOLEAN, album_id TEXT, thumbnail TEXT, medium_type TEXT, count INTEGER)');
      await db.execute('CREATE TABLE Images (id INTEGER PRIMARY KEY, image_id TEXT, name TEXT, path TEXT, thumbnail TEXT, album_id TEXT, is_tagged INTEGER, labels TEXT, like BOOLEAN, location TEXT, newest BOOLEAN DEFAULT \'false\')');
    });
    return db! ;
  }

  static Future<int> getCountFromTable(String table, {String suffix = ""}) async {
    var count = await runQuery("SELECT COUNT(id) FROM $table $suffix");
    if (count == null) return 0 ;
    return int.parse(count[0]['COUNT(id)'].toString()) ;
  }

  static Future<List<Map<String, Object?>>?> getAlbums({mainThread = true}) async {
    if (!mainThread){
      var databasesPath = await getDatabasesPath();
      String path = join(databasesPath, 'sensifai.db');
      db = await openDatabase(path, version: 1);
    }
    return await runQuery("SELECT * FROM Albums");
  }

  static Future<List<Map<String, Object?>>?> runQuery(String query) async {
    if (!query.contains("SELECT")){
      return null;
    }
    return await db?.rawQuery(query);
  }

  static Future<int?> modify(String query) async {
    if (query.startsWith("DELETE")){
      return await db?.rawDelete(query);
    } else if (query.startsWith("UPDATE")){
      return await db?.rawUpdate(query);
    } else if (query.startsWith("INSERT")){
      return await db?.rawInsert(query);
    } else {
      return null;
    }
  }

  static Future<dynamic> getAlbum(String id) async {
    var album = await runQuery("SELECT * FROM Albums WHERE album_id = $id");
    print(album);
  }

}