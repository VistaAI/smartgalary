
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_foreground_task/ui/with_foreground_task.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:path_provider/path_provider.dart';
import 'package:smartgallery/provider/database/database.dart';
import 'package:smartgallery/provider/notifiers/file_manager/file_manager_viewmodel.dart';
import 'package:smartgallery/provider/notifiers/gallery/album_viewmodel.dart';
import 'package:smartgallery/provider/notifiers/gallery/gallery_viewmodel.dart';
import 'package:smartgallery/provider/notifiers/gallery/image_viewmodel.dart';
import 'package:smartgallery/provider/notifiers/likes/likes_viewmodel.dart';
import 'package:smartgallery/provider/notifiers/navigation_viewmodel.dart';
import 'package:smartgallery/provider/notifiers/search/search_viewmodel.dart';
import 'package:smartgallery/provider/notifiers/settings/settings_viewmodel.dart';
import 'package:smartgallery/screens/navigation_screen.dart';
import 'package:watcher/watcher.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:smartgallery/provider/notifiers/on_boarding_viewmodel.dart';
import 'package:smartgallery/screens/onboarding/on_boarding_screen.dart';
import 'package:smartgallery/utils/permissions/permission_manager.dart';

void main() async {
  /*** Start initialing app. **/
  WidgetsBinding widgetsBinding = WidgetsFlutterBinding.ensureInitialized();
  FlutterNativeSplash.preserve(widgetsBinding: widgetsBinding);
  await EasyLocalization.ensureInitialized();
  await Hive.initFlutter();
  // const platform = MethodChannel('com.sensifai.smartgallery/ai');
  // await platform.invokeMethod("run");
  await Databases.initialDatabases();

  /*** End of initialing app. **/


  var directory = await getExternalStorageDirectory();
  if (kDebugMode) {
    print("Directory: $directory");
  }
  var watcher = DirectoryWatcher('/storage/emulated/0/');
  watcher.events.listen((event) {
    if (kDebugMode) {
      print("EVENT: $event");
    }
  });


  runApp(
    // Initial for multi language support with [EasyLocalization] dependency
      EasyLocalization(
        // List of language & countries app support
          supportedLocales: const [
            Locale('en')
          ],
          // start local from language
          startLocale: const Locale('en'),
          fallbackLocale: const Locale('en'),
          // select path of translation files
          path: 'assets/translations',
          // & Run root widget
          child:
          MultiProvider(
            providers: [
              // Initialing ChangeNotifierProvider's
              ChangeNotifierProvider(create: (_) => NavigationViewModel()),
              ChangeNotifierProvider(create: (_) => GalleryViewModel()),
              ChangeNotifierProvider(create: (_) => OnBoardingViewModel()),
              ChangeNotifierProvider(create: (_) => AlbumViewModel()),
              ChangeNotifierProvider(create: (_) => SettingsViewModel()),
              ChangeNotifierProvider(create: (_) => ImageViewModel()),
              ChangeNotifierProvider(create: (_) => LikesViewModel()),
              ChangeNotifierProvider(create: (BuildContext context) => SearchViewModel(context)),
              ChangeNotifierProvider(create: (BuildContext context) => FileManagerViewModel(context)),
            ],
            child: Application(await PermissionManager.isGranted(Permission.storage)),
          )
      )
  );
}



class Application extends StatefulWidget {
  final bool granted ;
  const Application(this.granted, {Key? key}) : super(key: key);

  @override
  State<Application> createState() => _ApplicationState();
}

class _ApplicationState extends State<Application> {

  @override
  Application get widget => super.widget;


  @override
  Widget build(BuildContext context) {
    FlutterNativeSplash.remove();
    return WithForegroundTask(
      child: Sizer(
          builder: (context, orientation, deviceType) {
            return MaterialApp(
              theme: ThemeData(
                  backgroundColor: const Color(0xFFFFFFFF),
                  fontFamily: 'productSans',
                  primaryColor: Colors.black,
                  primarySwatch: Colors.green,
                  dividerColor: Colors.black,
                  buttonColor: Colors.green,
                  primaryColorDark: Colors.green
              ),
              darkTheme: ThemeData(
                  backgroundColor: const Color(0xFF2B2B2B),
                  fontFamily: 'productSans',
                  primaryColor: Colors.white,
                  primarySwatch: Colors.green,
                  dividerColor: Colors.white,
                  buttonColor: Colors.green,
                  primaryColorDark: Colors.green
              ),
              title: 'Smart Gallery', // get translate of var with .tr() method
              // don't modify [localizationsDelegates] variables
              localizationsDelegates: context.localizationDelegates,
              // Add or delete supported language by modify [supportedLocales] variables
              supportedLocales: context.supportedLocales,
              // select current local, i recommend to edit start local from main function
              locale: context.locale,
              // & call to main class in home parameter
              home: !widget.granted ? const OnBoardingPage() : const Navigation(),
              // set dark or light theme
              themeMode: ThemeMode.system,
            );
          }
      ),
    );

  }
}
